#include <chrono>
#include <vector>
#include <math.h>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"

#include "interfaces/msg/radius_speed.hpp"


using namespace std::chrono_literals;
using std::placeholders::_1;

class PubSubTemplate : public rclcpp::Node
{
public:
PubSubTemplate() : Node("pub_sub_template")
{
        publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("current_pose", 10);

        pub_timer_ = this->create_wall_timer(
                100ms, std::bind(&PubSubTemplate::pub_timer_callback, this));

        subscription_ = this->create_subscription<interfaces::msg::RadiusSpeed>("radius_speed", 10, std::bind(&PubSubTemplate::topic_callback, this, _1));

}

private:

void topic_callback(const interfaces::msg::RadiusSpeed::SharedPtr msg)
{
        RCLCPP_INFO(this->get_logger(), "new cmd rcvd: speed = %lf  radius = %lf", msg->speed, msg->radius);

}

void pub_timer_callback()       // call this at steady rate to publish current vehicle pose
{
        geometry_msgs::msg::PoseStamped current_vehicle_pose;
        current_vehicle_pose.pose.position.x = 20.0;
        current_vehicle_pose.pose.position.y = 45.0;
        current_vehicle_pose.pose.position.z = 0.0;
        current_vehicle_pose.pose.orientation.x = 0.0;
        current_vehicle_pose.pose.orientation.y = 0.0;
        current_vehicle_pose.pose.orientation.z = sin((30.0*M_PI/180.0)/2.0);
        current_vehicle_pose.pose.orientation.w = cos((30.0*M_PI/180.0)/2.0);

        current_vehicle_pose.header.stamp = this->now();
        current_vehicle_pose.header.frame_id = "my_frame";

        publisher_->publish(current_vehicle_pose);

}

rclcpp::TimerBase::SharedPtr pub_timer_;
rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher_;

rclcpp::Subscription<interfaces::msg::RadiusSpeed>::SharedPtr subscription_;

};

int main(int argc, char** argv)
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<PubSubTemplate>());
        rclcpp::shutdown();
        return 0;
}

////////////////////////////////////////////////////////////////////////
