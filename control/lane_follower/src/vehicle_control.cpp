#include <chrono>
#include <vector>
#include <math.h>

#include "rclcpp/rclcpp.hpp"

#include "interfaces/msg/cdc_trajectory.hpp"  // input message
#include "interfaces/msg/radius_speed.hpp"    // output message
#include "nav_msgs/msg/path.hpp"               // output message

#include "cimar/support.h"
#include "cimar/support.cpp"

#include "cimar/route.h"
#include "cimar/route.cpp"

#include "cimar/pure_pursuit.h"
#include "cimar/pure_pursuit.cpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

class VehicleControl : public rclcpp::Node
{
public:
VehicleControl() : Node("vehicle_control")
{
        publisher_path_ = this->create_publisher<nav_msgs::msg::Path>("current_path", 10);


        publisher_radius_speed_ = this->create_publisher<interfaces::msg::RadiusSpeed>("radius_speed", 10);

        subscription_trajectory_ = this->create_subscription<interfaces::msg::CdcTrajectory>("current_trajectory", 10, std::bind(&VehicleControl::topic_callback, this, _1));

}

private:

void topic_callback(const interfaces::msg::CdcTrajectory::SharedPtr msg)
{
        void populate_path(nav_msgs::msg::Path *mypath, double vehicle_pt[3], double vehicle_heading, double look_ahead_pt[3], double look_ahead_heading, double D1, double W1);

        vehicle_pt[0] = msg->start_x;
        vehicle_pt[1] = msg->start_y;
        vehicle_heading = msg->start_heading;
        look_ahead_pt[0] = msg->end_x;
        look_ahead_pt[1] = msg->end_y;
        look_ahead_heading = msg->end_heading;
        my_radius = get_radius(vehicle_pt, vehicle_heading,
                               look_ahead_pt, look_ahead_heading, msg->dd1, msg->ww1);
        interfaces::msg::RadiusSpeed out_msg;
        out_msg.speed = msg->speed;
        out_msg.radius = my_radius;
        publisher_radius_speed_->publish(out_msg);
        //RCLCPP_INFO(this->get_logger(), "my_radius = %lf   msg.radius = %lf", my_radius, out_msg.radius) ;

        nav_msgs::msg::Path my_path;
        my_path.header.stamp = this->now();
        my_path.header.frame_id = "my_frame";
        populate_path(&my_path, vehicle_pt, vehicle_heading, look_ahead_pt, look_ahead_heading, msg->dd1, msg->ww1);
        publisher_path_->publish(my_path);
}

rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_path_;
rclcpp::Publisher<interfaces::msg::RadiusSpeed>::SharedPtr publisher_radius_speed_;

rclcpp::Subscription<interfaces::msg::CdcTrajectory>::SharedPtr subscription_trajectory_;

double my_radius;
double vehicle_pt[3] = {0.0,0.0,0.0};
double vehicle_heading;
double look_ahead_pt[3] = {0.0,0.0,0.0};
double look_ahead_heading;

};

int main(int argc, char** argv)
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<VehicleControl>());
        rclcpp::shutdown();
        return 0;
}

////////////////////////////////////////////////////////////////////////
void populate_path(nav_msgs::msg::Path *mypath, double vehicle_pt[3], double vehicle_heading, double look_ahead_pt[3], double look_ahead_heading, double D1, double W1)
{
        void get_midpoint(double mid_pt[3], double *mid_pt_heading, double M1[3], double M2[3], double vehicle_pt[3], double vehicle_heading, double look_ahead_pt[3], double look_ahead_heading, double d1);


        struct route_segment_struct route_seg;
        int i;
        double u, mid_pt[3], mid_pt_heading, M1[3], M2[3], my_pt[3];
        double radius, dxdu, dydu ;
        get_midpoint(mid_pt, &mid_pt_heading, M1, M2,
                     vehicle_pt, vehicle_heading, look_ahead_pt, look_ahead_heading, D1);

        geometry_msgs::msg::PoseStamped this_pose;
        // load the start pose in the path listing
        this_pose.pose.position.x = vehicle_pt[0];
        this_pose.pose.position.y = vehicle_pt[1];
        this_pose.pose.position.z = vehicle_pt[2];
        this_pose.pose.orientation.x = 0.0;
        this_pose.pose.orientation.y = 0.0;
        this_pose.pose.orientation.z = sin(vehicle_heading/2.0);
        this_pose.pose.orientation.w = cos(vehicle_heading/2.0);

        mypath->poses.push_back(this_pose);

        for (i=0; i<3; ++i)
        {
                route_seg.p0[i] = vehicle_pt[i];
                route_seg.p1[i] = M1[i];
                route_seg.p2[i] = mid_pt[i];
                route_seg.w1 = W1;
        }

        for (u=0.0; u<=1.0; u+=0.05)
        {
                get_pt_on_route(my_pt, &route_seg, u);
                get_slope_and_curvature_on_route(&dxdu, &dydu, &radius, &route_seg, u);
                this_pose.pose.position.x = my_pt[0];
                this_pose.pose.position.y = my_pt[1];
                this_pose.pose.position.z = my_pt[2];
                this_pose.pose.orientation.x = 0.0;
                this_pose.pose.orientation.y = 0.0;
                this_pose.pose.orientation.z = sin(atan2(dydu,dxdu)/2.0);
                this_pose.pose.orientation.w = cos(atan2(dydu,dxdu)/2.0);

                mypath->poses.push_back(this_pose);
        }


        // load the midpoint pose in the path listing
        this_pose.pose.position.x = mid_pt[0];
        this_pose.pose.position.y = mid_pt[1];
        this_pose.pose.position.z = mid_pt[2];
        this_pose.pose.orientation.x = 0.0;
        this_pose.pose.orientation.y = 0.0;
        this_pose.pose.orientation.z = sin(mid_pt_heading/2.0);
        this_pose.pose.orientation.w = cos(mid_pt_heading/2.0);

        mypath->poses.push_back(this_pose);

        for (i=0; i<3; ++i)
        {
                route_seg.p0[i] = mid_pt[i];
                route_seg.p1[i] = M2[i];
                route_seg.p2[i] = look_ahead_pt[i];
                route_seg.w1 = W1;
        }

        for (u=0.0; u<=1.0; u+=0.05)
        {
                get_pt_on_route(my_pt, &route_seg, u);
                get_slope_and_curvature_on_route(&dxdu, &dydu, &radius, &route_seg, u);
                this_pose.pose.position.x = my_pt[0];
                this_pose.pose.position.y = my_pt[1];
                this_pose.pose.position.z = my_pt[2];
                this_pose.pose.orientation.x = 0.0;
                this_pose.pose.orientation.y = 0.0;
                this_pose.pose.orientation.z = sin(atan2(dydu,dxdu)/2.0);
                this_pose.pose.orientation.w = cos(atan2(dydu,dxdu)/2.0);

                mypath->poses.push_back(this_pose);
        }

        // load the endpoint pose in the path listing
        this_pose.pose.position.x = look_ahead_pt[0];
        this_pose.pose.position.y = look_ahead_pt[1];
        this_pose.pose.position.z = look_ahead_pt[2];
        this_pose.pose.orientation.x = 0.0;
        this_pose.pose.orientation.y = 0.0;
        this_pose.pose.orientation.z = sin(look_ahead_heading/2.0);
        this_pose.pose.orientation.w = cos(look_ahead_heading/2.0);

        mypath->poses.push_back(this_pose);
}
////////////////////////////////////////////////////////////////////////
