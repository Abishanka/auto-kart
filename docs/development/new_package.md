# New Package WIP
If you are thinking about creating a new package for the project there are a couple of considerations

## Domain
Where does the software fit within the standard autonomy divisions?
* perception
* planning
* control
* drivers
* utility

## Programming Language
C++ is the preferred for critical path code. Python can be suitable for small programs or utilities but not for computationally expensive programs or complex programs where test suites are hard to produce to cover the entire code.

## Creating a new ROS2 package
To create a new package, navigate to the desired sub-directory(control, planner, etc.). From there you can follow the official [ROS2 tutorial](https://index.ros.org/doc/ros2/Tutorials/Creating-Your-First-ROS2-Package/) when creating a new package.

A great place to get a skeleton template for a ROS2 node is the official [ROS2 examples](https://github.com/ros2/examples) github repo.

You can also view other packages in the repo as examples, but there is not gurantee that they are good examples.