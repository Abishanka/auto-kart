# Working with the documentation - WIP
If you write any code for the repository it is important to write documentation. The documentation should include general
usage instructions and an summary of the principles used in the code.

## File Types
The documentation can be written using either markdown (.md) or restructured text (.rst) formats.

## Notes
* It is important that any new files created for the documentation are included in a toctree specified in a .rst file. You can use the existing documentation as an example.

## Build
You can build the documentation by being in the root of the repostiroy e.g. ``` ~/workspace_name/src/indy_av``` and typing the command:
```bash
./scripts/build_docs.sh
```
You can then view the documentation locally using:
```bash
./scripts/view_docs.sh
```
## Notes
If the built documentation is not full updating try running:
```bash
make clean
```
in the docs folder of the repo. This will remove all built files so the build will regenerate everything.