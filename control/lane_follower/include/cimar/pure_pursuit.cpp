// Carl Crane
// University of Florida
// 20 Feb 2020

#include <iostream>
#include <math.h>
using namespace std;

#include "route.h"
#include "pure_pursuit.h"
#include "support.h"

////////////////////////////////////////////////////////////////////////////////////
void get_look_ahead_point(// outputs
                          double look_ahead_pt[3],
                          double *look_ahead_heading_rad,
                          double my_closest_pt_lines[3],
                          int *my_closest_segment_num,
                          double my_closest_pt_curve[3],
                          double *heading_closest_pt_curve,
                          // inputs
                          double look_ahead_dist,
                          double vehicle_pt[3],
                          int num_segments,
                          struct route_segment_struct route_segments[])
{
	// inputs -
	//     look_ahead_dist: the distance from the closest point on the route to the look ahead point
	//     vehicle_pt: coordinates of the vehicle at this instant
	//     num_segments: number of route segments
	//     route_segments: an array of route_segment_struct
	//
	// outputs -
	//     look_ahead_pt: coordinates of point that is 'look_ahead_dist' from the closest point on the curved route
	//     look_ahead_heading_rad: pointer to direction tangent to route at the look ahead point, measured from x axis
	//     my_closest_pt_lines:  the point on the straight line segments that is closest to the vehicle point
	//     *my_closest_segment :  the segment number on which the point on the straight line segments that is closest to the vehicle point lies
	//     my_closest_pt_curve:  the point on the curved route that is closest to the vehicle point
  //     heading_closest_pt_curve:  the headging angle in radians at the closest point on the route segment

  double dxdu_at_closest, dydu_at_closest, radius_at_closest ;

  void get_overall_closest_point(double overall_closest_pt_lines[3], double *overall_closest_u, int *overall_seg_num,
			double vehicle_pt[3], int num_segments, struct route_segment_struct route_segments[]) ;

	double  overall_closest_u ;

	get_overall_closest_point(my_closest_pt_lines, &overall_closest_u, my_closest_segment_num, vehicle_pt, num_segments, route_segments) ;
	get_pt_on_route(my_closest_pt_curve, &(route_segments[*my_closest_segment_num]), overall_closest_u) ;

  // get heading at closest point on path
  get_slope_and_curvature_on_route(&dxdu_at_closest, &dydu_at_closest, &radius_at_closest, &(route_segments[*my_closest_segment_num]), overall_closest_u) ;
  *heading_closest_pt_curve = atan2(dydu_at_closest, dxdu_at_closest) ;

	// now get look-ahead point as being look_ahead_dist 'upstream' from the overall_closest_point

	double dist_offset, dist_remaining, dist_from_p0;
	dist_remaining = look_ahead_dist ;

	int look_ahead_seg_num ;
	double look_ahead_u ;

	dist_offset = get_route_length_to_u(&(route_segments[*my_closest_segment_num]), overall_closest_u) ;
	look_ahead_seg_num = *my_closest_segment_num ;

	dist_from_p0 = dist_remaining + dist_offset ;



	int num_jump_ahead = 0 ;
	int jump_ahead_seg_num ;
	jump_ahead_seg_num = *my_closest_segment_num ;

	while (dist_from_p0 > route_segments[jump_ahead_seg_num].length)
	{
		dist_from_p0 -= route_segments[jump_ahead_seg_num].length ;
		jump_ahead_seg_num = (jump_ahead_seg_num+1)%num_segments ;
		num_jump_ahead++ ;
	}

	look_ahead_seg_num = *my_closest_segment_num ;
	look_ahead_seg_num = (*my_closest_segment_num + num_jump_ahead)%num_segments ;
	dist_from_p0 = dist_remaining + dist_offset ;


	int i ;

	for (i=0 ; i< num_jump_ahead ; ++i)
	{
		dist_from_p0 = dist_from_p0 - route_segments[(*my_closest_segment_num+i)%num_segments].length ;
	}

	look_ahead_u = get_uval_for_dist(&(route_segments[look_ahead_seg_num]), dist_from_p0) ;
	//ROS_INFO("pure_pursuit: look_ahead_u = %lf deg  look_ahead_seg = %d", look_ahead_u, look_ahead_seg_num) ;
	// now calculate the look-ahead point and get its heading

	get_pt_on_route(look_ahead_pt, &(route_segments[look_ahead_seg_num]), look_ahead_u) ;

	double dxdu, dydu, radius ;
	get_slope_and_curvature_on_route(&dxdu, &dydu, &radius, &(route_segments[look_ahead_seg_num]), look_ahead_u) ;
	*look_ahead_heading_rad = atan2(dydu, dxdu) ;

}


////////////////////////////////////////////////////////////////////////////////////
void calculate_closest_point_on_infinite_line(double closest_point[3], double vehicle_point[3], double Svec[3], double SOL[3])
{
	// equation 1.127 on page 35 of text
	// inputs:
	//      vehicle_point - xyz coordinate of vehicle at current time, units of make_heap
	//      Svec, SOL     - coordinates of an infinite line (Svec does not have to be a unit vector)
	// outputs:
	//      closest_point - xyz coorinates of point on infinite line that is closest to the vehicle point

	int i ;
	double dvec[3] ;
	double temp1[3], temp2[3], numer[3], denom ;

	cdc_cross(temp1, vehicle_point, Svec) ;
	for(i=0 ; i<3 ; ++i)
	{
		temp2[i] = SOL[i] - temp1[i] ;
	}

	cdc_cross(numer, Svec, temp2) ;
	denom = cdc_dot(Svec, Svec) ;

	for(i=0 ; i<3 ; ++i)
	{
		dvec[i] = numer[i]/denom ;
	}

	for(i=0 ; i<3 ; ++i)
	{
		closest_point[i] = vehicle_point[i] + dvec[i] ;
	}
}


////////////////////////////////////////////////////////////////////////////////////
int is_point_on_segment(double pt[3], double segpt1[3], double segpt2[3])
{
	// inputs -
	//       segpt1, segpt2 - xyz coordinates of the endpoints of a line segment
	//       pt - xyz of a point on the infintite line
	// outputs -
	//      none
	// return -
	//      returns 1 if the point is on the line segment; 0 otherwise

	double S1[3], S2[3] ;
	int i ;
	for (i=0 ; i<3 ; ++i)
	{
		S1[i] = pt[i] - segpt1[i] ;
		S2[i] = pt[i] - segpt2[i] ;
	}

	if (cdc_dot(S1,S2) > 0)
	return (0) ;
	else
	return (1) ;
}

////////////////////////////////////////////////////////////////////////////////////
void get_closest_point(double ans[3], struct route_segment_struct *p, double veh_point[3])
{
	// inputs -
	//      p - pointer to a line segment structure
	//      veh_point - xyz coordinates of the vehicle location
	// outputs -
	//      ans - xyz coordinates of the point on the line segment that is closest to the vehicle point
	double temp1[3], temp2[3], dist1, dist2 ;
	double Svec[3], SOL[3], closest_point[3] ;

	int j ;

	for (j=0 ; j<3 ; ++j)
	Svec[j] = p->p2[j] - p->p0[j] ;

	cdc_cross(SOL, p->p0, Svec) ;

	calculate_closest_point_on_infinite_line(closest_point, veh_point, Svec, SOL) ;

	if (is_point_on_segment(closest_point, p->p0, p->p2))
	{
		for (j=0 ; j<3 ; ++j)
		{
			ans[j] = closest_point[j] ;
		}
	}
	else
	{
		for (j=0 ; j<3 ; ++j)
		{
			temp1[j] = p->p0[j] - veh_point[j] ;
			temp2[j] = p->p2[j] - veh_point[j] ;
		}
		dist1 = cdc_mag(temp1) ;
		dist2 = cdc_mag(temp2) ;

		if (dist1 < dist2)
		for (j=0 ; j<3 ; ++j)
		ans[j] = p->p0[j] ;
		else
		for (j=0 ; j<3 ; ++j)
		ans[j] = p->p2[j] ;
	}
}

////////////////////////////////////////////////////////////////////////////////////
void get_overall_closest_point(double overall_closest_pt_lines[3], double *pu, int *p_overall_closest_segnum,
			double vehicle_pt[3], int num_segments, struct route_segment_struct route_segments[])
{
	// inputs -
	//       vehicle_pt - xyz coordinates of the vehicle at this instant, units of m
	//       num_segs  - number of line segments that comprise the closed-loop path
	//       route_segments   - the array of segment structures that define the path
	// outputs -
	//       p_overall_closest_segnum - pointer to an int that stores the segment number
	//		 pu - pointer to the value of u that corresponds to the closest point on the path
	//       overall_closest_point_lines - xyz coordinates of the point on the entire path (straight line segments
	//               that is closest to the vehicle position

	int i, j, best_overall_seg ;
	double best_overall_dist = 999999.0 ;
	double best_closest_point[3], this_closest_point[3], this_closest_dist, vec_to_closest[3] ;
	double mag ;

	void get_closest_point(double ans[3], struct route_segment_struct *p, double veh_point[3]) ;

	void get_slope_and_curvature_on_route(double *dxdu, double *dydu, double *radius, struct route_segment_struct *segment, double u) ;

	for (i=0 ; i< num_segments ; ++i)
	{
		get_closest_point(this_closest_point, &(route_segments[i]), vehicle_pt) ;
		for (j=0 ; j<3 ; ++j)
		{
			vec_to_closest[j] = this_closest_point[j] - vehicle_pt[j] ;
		}
		this_closest_dist = cdc_mag(vec_to_closest) ;

		if (this_closest_dist < best_overall_dist)
		{
			best_overall_dist = this_closest_dist ;
			best_overall_seg = i ;
			for (j=0 ; j < 3 ; ++j)
			{
				best_closest_point[j] = this_closest_point[j] ;
			}
		}
	}
	for (j=0 ; j<3 ; ++j)
	{
		overall_closest_pt_lines[j] = best_closest_point[j] ;
	}
	*p_overall_closest_segnum = best_overall_seg ;

	double temp[3], dist ;
	// check if the overall_closest_segment is a straight line portion of the route
	if (route_segments[*p_overall_closest_segnum].is_straight_seg == 1)
	{
		for (j=0 ; j<3 ; ++j)
		{
			temp[j] = overall_closest_pt_lines[j] - route_segments[*p_overall_closest_segnum].p0[j] ;
		}

		dist = cdc_mag(temp) ;
		*pu = get_uval_for_dist(&(route_segments[*p_overall_closest_segnum]), dist) ;

		return ;
	}

	// now find true closest point (get value for u for this segment)
	double u, best_u, denom, current_pt[3], p0[3], p1[3], p2[3], Svec[3] ;
	for (j=0 ; j<3 ; ++j)
	{
		p0[j] = route_segments[*p_overall_closest_segnum].p0[j] ;
		p1[j] = route_segments[*p_overall_closest_segnum].p1[j] ;
		p2[j] = route_segments[*p_overall_closest_segnum].p2[j] ;
	}

	best_u = 0.0 ;

	double best_mag = 9999.0 ;

	for (u=0.0 ; u<=1.0 ; u+=0.001)  // was +=0.0001
	{
		denom = (1-u)*(1-u) + 2.0*u*(1-u)*route_segments[*p_overall_closest_segnum].w1 + u*u ;
		for (j=0 ; j<3 ; ++j)
		{
			current_pt[j] = ((1-u)*(1-u)*p0[j] + 2.0*u*(1-u)*route_segments[*p_overall_closest_segnum].w1*p1[j] + u*u*p2[j])/denom ;
			Svec[j] = current_pt[j] - vehicle_pt[j] ;
			mag = cdc_mag(Svec) ;
		}

		if (fabs(mag) < 0.025)
		{
			// we are on the route ; no need to look for a better value of u
			best_u = u ;
			break ;
		}

		if (fabs(mag) < best_mag)
		{
			best_mag = fabs(mag) ;
			best_u = u ;
		}

	}

	*pu = best_u ;
}

////////////////////////////////////////////////////////////////////////////////////

double get_radius(double vehicle_pt[3], double vehicle_heading,
                  double look_ahead_pt[3], double look_ahead_heading, double d1, double w1)
{
	// A path from the current vehicle location to the look ahead point is created from two route
	// segments.  See section 5 (Determination of Instantaneous Steering Angle) of the Route Planning
	// Document.  The radius of curvature at the start of the first route segment is calculated and
	// is returned by this function.

	// inputs -
	//     vehicle_pt: coordinates of the vehicle at this instant
	//     vehicle_heading: current orientation of the vehicle (radians) from the x axis, about the z axis
	//     look_ahead_pt: coordinates of the look ahead point
	//     look_ahead_heading: desired orientation at the look ahead point
	//     d1:  A distance used to calculate points M1 and M2 (see Route Planning Document).  Typically
	//          set equal to one fourth of the look ahead distance that was used to get the look ahead
	//          point.
	//     w1:  A dimensionless scalar that defines the shape of the route segments.  Typically equals 1.
	//
	// outputs -
	//     The radius of curvature at the start of the path is returned.  A positive value means turn left
	//     (need to double check this).  A value of 9999.0 is returned if the vehicle should go straight
	//     ahead, i.e. maintain its current orientation.

	void get_midpoint(double mid_pt[3], double *mid_pt_heading, double M1[3], double M2[3],
                  double vehicle_pt[3], double vehicle_heading,
				  double look_ahead_pt[3], double look_ahead_heading, double d1) ;

	double calc_radius_at_start(double P0[3], double P1[3], double P2[3], double w1) ;

	double mid_pt[3], mid_pt_heading, M1[3], M2[3], radius_ans ;

	get_midpoint(mid_pt, &mid_pt_heading, M1, M2,
                  vehicle_pt, vehicle_heading, look_ahead_pt, look_ahead_heading, d1) ;
	radius_ans = calc_radius_at_start(vehicle_pt, M1, mid_pt, w1) ;

	return radius_ans ;
}

////////////////////////////////////////////////////////////////////////
void get_midpoint(double mid_pt[3], double *mid_pt_heading, double M1[3], double M2[3],
                  double vehicle_pt[3], double vehicle_heading, double look_ahead_pt[3], double look_ahead_heading, double d1)
{
	// 2D case
	int j ;
	double Svec[3], mag ;

	M1[0] = vehicle_pt[0] + d1*cos(vehicle_heading) ;
	M1[1] = vehicle_pt[1] + d1*sin(vehicle_heading) ;
	M1[2] = vehicle_pt[2] ;
	M1[2] = 0.0 ;

	M2[0] = look_ahead_pt[0] - d1*cos(look_ahead_heading) ;
	M2[1] = look_ahead_pt[1] - d1*sin(look_ahead_heading) ;
	M2[2] = look_ahead_pt[2] ;
	M2[2] = 0.0 ;

	for (j=0 ; j<3 ; ++j)
	{
		Svec[j] = M2[j] - M1[j] ;
	}

	mag = cdc_mag(Svec) ;

	for (j=0 ; j<3 ; ++j)
	{
		Svec[j] = Svec[j]/mag ;
		mid_pt[j] = (M1[j]+M2[j])/2.0 ;
	}
	*mid_pt_heading = atan2(Svec[1], Svec[0]) ;
}
////////////////////////////////////////////////////////////////////////
double calc_radius_at_start(double P0[3], double P1[3], double P2[3], double w1)
{
	double p0x, p0y, p1x, p1y, p2x, p2y ;
	double numer, denom, term1 ;

	p0x = P0[0] ; p0y = P0[1] ;
	p1x = P1[0] ; p1y = P1[1] ;
	p2x = P2[0] ; p2y = P2[1] ;

	term1 = (p0x*p0x+p0y*p0y)*w1*w1 - 2.0*(p0x*p1x+p0y*p1y)*w1 + (p1x*p1x+p1y*p1y) ;

	numer = -2.0*pow(term1, 1.5) ;

	denom = (p0x*p2y-p0y*p2x)*w1 + (p0y*p1x-p0x*p1y) + (p1y*p2x-p1x*p2y) ;

	if (cdc_value_near(denom, 0.0, 0.0001))
		return (9999.0) ;  // infinite radius, go straight

	return (numer/denom) ;
}
////////////////////////////////////////////////////////////////////////
