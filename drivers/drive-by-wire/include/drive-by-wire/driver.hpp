#pragma once

#include "serial/serial.h"
#include <string>

/*
 * This file will contain the base class to be used for each serial device used for the drive-by-wire functionality
 * Derived classes must implement the virtual function provided. They should store their own important state information.
 */

class ActuationDriver : public serial::Serial
{
    protected:
        bool Actuating = false;

    public:
        ActuationDriver(std::string port, unsigned long baud, int timeout_ms)
            : Serial(port, baud, serial::Timeout::simpleTimeout(timeout_ms))
        {

        }

        /*
         * This was added for the purpose of knowing when an actuator hits it set point. This is to avoid
         * potential scenarios when the throttle is actuating but the brake is in the process of actuating
         * to a zero effort position or the other way around. May not be needed.
         */ 
        bool isActuating()
        {
            return Actuating;
        }

        /*
         * This function function should be overriden to have specifics of the communcations with the
         * appropriate actuator
         */
        virtual void sendMessage() = 0;

        /*
         * This function was intended to provide some kind of status on whether the driver is operational
         * or not. This may not be used and could be removed.
         */
        virtual std::string driverStatus() = 0;

        /*
         * This function should be the only function called to change the state of the driver.
         * effort value should range from [0, 1].
         */ 
        virtual void updateEffort(double effort) = 0;

        /*
         * This function is responsible for calling the correct serial read function and parsing the read data
         * into the appropriate variables.
         */ 
        virtual void readIncomingMessage() = 0;

        /*
         * This function should contain the functionality to receive the updates from the actuators and send 
         * new data to the actuators. This function is intended to run every loop iteration in a main program.
         * I/O writes and reads should block program execution for as litle time as possible.
         */ 
        virtual void update() = 0;
        
};
