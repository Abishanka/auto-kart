#include <math.h>
#include "driver.hpp"

#define COUNTS_PER_REV 4000 // Encoder counts per rev, depends on motor
#define GR 16               // Should be > 1

#define MAX_COUNT 40000     // Commands to motor should never request a encoder count higher than this
#define MIN_COUNT -55000    // This value should match the offset in the motor code

/*
 * This driver is for use with an SM2335D SmartMotor running the appropriate .sms script.
 *  Input:
 *      * The input to this driver should be a quantity of effort [0,1]. It is up to the driver to determine
 *          how an actuation effort is mapped to a setpoint
 * 
 *  Notes:
 *      * Code will clamp the max encoder count value sent to the motor instead of tossing the command
 *          and providing feedback.
 */

class BrakeSmartMotorDriver : public ActuationDriver
{
    private:
        float desiredCount = 0;
        bool newEffort = false;
        bool firstTime = true;
	    bool invalid_arg = false;
        
    public:
        BrakeSmartMotorDriver(std::string port, unsigned long baud, int timeout_ms)
            : ActuationDriver(port, baud, timeout_ms)
        {

        }

        void sendMessage() override
        {
            if(isOpen())
            {
                int encoderCount = (int)round(desiredCount);
                if(encoderCount > MAX_COUNT){ encoderCount = MAX_COUNT; }
                else if(encoderCount < MIN_COUNT){ encoderCount = MIN_COUNT; }
                std::string outputMessage = "p=" + std::to_string(encoderCount) + "\r";
                write(outputMessage);
            }
        }

        std::string driverStatus() override
        {
            // This function returns to main code whether input effort was valid or not
            std::string status;

            if(!invalid_arg){ status = "Drivers have run"; }
            else { status = "Invalid Argument"; invalid_arg = false; }

            return status;
        }

        void updateEffort(double effort) override
        {
            // Input should be a int beteen [0, 1]
            if(effort > 1 || effort < 0)
            { 
                invalid_arg = true;
                return; 
	        }
             
            desiredCount = MAX_COUNT*effort; // Maps effort value range to MAX_COUNT value range
            newEffort = true;
        }

        void readIncomingMessage() override
        {
            // What happens if multiple messages are in the queue?
            // readline will only pop of up to the first carriage return and leave the rest
            std::string readString = readline(20, "\r");
            // TODO: parse any received data, if any.
        }

        void update() override
        {
            if(firstTime)
            {
                write("RUN\r");
                firstTime = false;
            }

            if(isOpen())
            {
                // Don't try to read if nothing is there.
                if(available())
                {
                    readIncomingMessage();
                }

                if(newEffort)
                {
                    sendMessage();  
                    newEffort = false;
                }
            }
        }
};
