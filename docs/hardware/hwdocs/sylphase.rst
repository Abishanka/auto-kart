Syphase Infix-1
===============
`Sylpase Infix-1 GNSS/INS <https://sylphase.com/technology.html>`_

Connectors
----------
USB-C cable

Power
-----
Power through USB connection.

Accessories
-----------
The antenna used is a NovAtel antenna (include a reference)