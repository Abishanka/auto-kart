Velodyne VLP-16
===============
`Velodyne Puck (VLP-16) <https://velodynelidar.com/products/puck/>`_

Connectors
----------
* Ethernet
* Barrel jack power plug

Configuration
-------------
* IP address
