#include <chrono>
#include <vector>
#include <math.h>
#include <stdlib.h>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"   // input message - current pose
#include "nav_msgs/msg/path.hpp"                // input message - optimal route
#include "std_msgs/msg/float32_multi_array.hpp" // input message - speeds at route points

#include "interfaces/msg/cdc_trajectory.hpp"  // output message
#include "visualization_msgs/msg/marker.hpp"       // output chosen lane as text

#include "cimar/support.h"
#include "cimar/support.cpp"

#include "cimar/route.h"
#include "cimar/route.cpp"

#include "cimar/pure_pursuit.h"
#include "cimar/pure_pursuit.cpp"

#define MAX_POINTS 200
#define LOOK_AHEAD_DIST 40.0
#define SPEED 20.0
#ifndef myW1
#define myW1 1.0
#endif

using namespace std::chrono_literals;
using std::placeholders::_1;

class Get_Carrot : public rclcpp::Node
{
public:
Get_Carrot() : Node("get_carrot")
{
        publisher_cdc_text_ = this->create_publisher<visualization_msgs::msg::Marker>("print_cdc_text", 10);

        publisher_trajectory_ = this->create_publisher<interfaces::msg::CdcTrajectory>("current_trajectory", 10);

        publisher_carrot_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("current_carrot", 10);

        subscription_path_ = this->create_subscription<nav_msgs::msg::Path>("optimal_route", 10, std::bind(&Get_Carrot::topic_callback_optimal_path, this, _1));
        subscription_lane1_ = this->create_subscription<nav_msgs::msg::Path>("lane1", 10, std::bind(&Get_Carrot::topic_callback_lane1, this, _1));
        subscription_lane2_ = this->create_subscription<nav_msgs::msg::Path>("lane2", 10, std::bind(&Get_Carrot::topic_callback_lane2, this, _1));
        subscription_lane3_ = this->create_subscription<nav_msgs::msg::Path>("lane3", 10, std::bind(&Get_Carrot::topic_callback_lane3, this, _1));
        subscription_lane4_ = this->create_subscription<nav_msgs::msg::Path>("lane4", 10, std::bind(&Get_Carrot::topic_callback_lane4, this, _1));
        subscription_pose_ = this->create_subscription<geometry_msgs::msg::PoseStamped>("current_pose", 10, std::bind(&Get_Carrot::topic_callback_pose, this, _1));
        subscription_speeds_ = this->create_subscription<std_msgs::msg::Float32MultiArray>("optimal_route_speeds", 10, std::bind(&Get_Carrot::topic_callback_speeds, this, _1));

        timer_ = this->create_wall_timer(
                6000ms, std::bind(&Get_Carrot::timer_callback, this)); // every 6 seconds
}

private:

void timer_callback()
{
        // randomly pick a 'chosen route' every 6 seconds
        //chosen_route = rand()%5;

        static int last_chosen = -4;
        chosen_route = rand()%4 + 1;   // don't jump on the optimal route, route 0
        while(chosen_route == last_chosen)
                chosen_route = rand()%4 +1;
        last_chosen =  chosen_route;

        cdc_text.header.frame_id = "vehicle";
        cdc_text.header.stamp = this->now();
        cdc_text.ns = "print chosen path";
        cdc_text.action = visualization_msgs::msg::Marker::ADD;
        cdc_text.id = 9;   // view oriented text
        cdc_text.type = visualization_msgs::msg::Marker::TEXT_VIEW_FACING;

        cdc_text.color.r = 1.0;
        cdc_text.color.g = 1.0;
        cdc_text.color.b = 1.0;
        cdc_text.color.a = 1.0;
        cdc_text.scale.x = 10.0;
        cdc_text.scale.y = 10.0;
        cdc_text.scale.z = 10.0;

        cdc_text.pose.position.x =  150.0;
        cdc_text.pose.position.y =  150.0;

        char mychar[40];
        sprintf(mychar, "route = %d", chosen_route);
        cdc_text.text = mychar;

        publisher_cdc_text_->publish(cdc_text);

}

void topic_callback_speeds(const std_msgs::msg::Float32MultiArray::SharedPtr msg)
{
        int i, num_pts;

        if (have_optimal_route_segments)
        {
                num_pts = num_optimal_route_segments + 1; //careful here with array size

                have_route_segment_speeds = 1; // set to true so other callback
                                               // knows we are good to go
                for(i=0; i<num_pts; ++i)
                {
                        optimal_route_points[i].speed = msg->data[i];
                }
        }
}

void topic_callback_optimal_path(const nav_msgs::msg::Path::SharedPtr msg)
{
        // read in the optimal path and create the segment structures
        int i, num_pts;
        num_pts = msg->poses.size();
        // RCLCPP_INFO(this->get_logger(), "num_pts = %d yes", num_pts) ;

        if (num_pts > MAX_POINTS || num_pts<1)
        {
                //  RCLCPP_INFO(this->get_logger(), "get_carrot callback 1: num_pts = %d", num_pts) ;
                exit (9);
        }

        num_optimal_route_segments = num_pts - 1;

        for (i=0; i < num_pts; ++i)
        {
                optimal_route_points[i].pt[0] = msg->poses[i].pose.position.x;
                optimal_route_points[i].pt[1] = msg->poses[i].pose.position.y;
                optimal_route_points[i].pt[2] = msg->poses[i].pose.position.z;
                optimal_route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z, msg->poses[i].pose.orientation.w);
                optimal_route_points[i].w1_for_subsequent_segment = myW1;
//RCLCPP_INFO(this->get_logger(), "carrot callback 1: route_pt %d heading = %lf deg", i, route_points[i].heading_rad*180.0/M_PI) ;
        }

        create_route_segments(optimal_route_segments, optimal_route_points, num_pts);

        have_optimal_route_segments = 1;    // set to true so other callback
        // knows we are good to go
}

void topic_callback_lane1(const nav_msgs::msg::Path::SharedPtr msg)
{
        // read in the lane1 path and create the segment structures
        int i, num_pts;
        num_pts = msg->poses.size();
        // RCLCPP_INFO(this->get_logger(), "num_pts = %d yes", num_pts) ;

        if (num_pts > MAX_POINTS || num_pts<1)
        {
                //  RCLCPP_INFO(this->get_logger(), "get_carrot callback 1: num_pts = %d", num_pts) ;
                exit (9);
        }

        num_lane1_route_segments = num_pts - 1;

        for (i=0; i < num_pts; ++i)
        {
                lane1_route_points[i].pt[0] = msg->poses[i].pose.position.x;
                lane1_route_points[i].pt[1] = msg->poses[i].pose.position.y;
                lane1_route_points[i].pt[2] = msg->poses[i].pose.position.z;
                lane1_route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z, msg->poses[i].pose.orientation.w);
                lane1_route_points[i].w1_for_subsequent_segment = myW1;
//RCLCPP_INFO(this->get_logger(), "carrot callback 1: route_pt %d heading = %lf deg", i, route_points[i].heading_rad*180.0/M_PI) ;
        }

        create_route_segments(lane1_route_segments, lane1_route_points, num_pts);

        have_lane1_segments = 1;    // set to true so other callback
                                    // knows we are good to go
}

void topic_callback_lane2(const nav_msgs::msg::Path::SharedPtr msg)
{
        // read in the lane1 path and create the segment structures
        int i, num_pts;
        num_pts = msg->poses.size();
        // RCLCPP_INFO(this->get_logger(), "num_pts = %d yes", num_pts) ;

        if (num_pts > MAX_POINTS || num_pts<1)
        {
                //  RCLCPP_INFO(this->get_logger(), "get_carrot callback 1: num_pts = %d", num_pts) ;
                exit (9);
        }

        num_lane2_route_segments = num_pts - 1;

        for (i=0; i < num_pts; ++i)
        {
                lane2_route_points[i].pt[0] = msg->poses[i].pose.position.x;
                lane2_route_points[i].pt[1] = msg->poses[i].pose.position.y;
                lane2_route_points[i].pt[2] = msg->poses[i].pose.position.z;
                lane2_route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z, msg->poses[i].pose.orientation.w);
                lane2_route_points[i].w1_for_subsequent_segment = myW1;
//RCLCPP_INFO(this->get_logger(), "carrot callback 1: route_pt %d heading = %lf deg", i, route_points[i].heading_rad*180.0/M_PI) ;
        }

        create_route_segments(lane2_route_segments, lane2_route_points, num_pts);

        have_lane2_segments = 1;    // set to true so other callback
                                    // knows we are good to go
}

void topic_callback_lane3(const nav_msgs::msg::Path::SharedPtr msg)
{
        // read in the lane1 path and create the segment structures
        int i, num_pts;
        num_pts = msg->poses.size();
        // RCLCPP_INFO(this->get_logger(), "num_pts = %d yes", num_pts) ;

        if (num_pts > MAX_POINTS || num_pts<1)
        {
                //  RCLCPP_INFO(this->get_logger(), "get_carrot callback 1: num_pts = %d", num_pts) ;
                exit (9);
        }

        num_lane3_route_segments = num_pts - 1;

        for (i=0; i < num_pts; ++i)
        {
                lane3_route_points[i].pt[0] = msg->poses[i].pose.position.x;
                lane3_route_points[i].pt[1] = msg->poses[i].pose.position.y;
                lane3_route_points[i].pt[2] = msg->poses[i].pose.position.z;
                lane3_route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z, msg->poses[i].pose.orientation.w);
                lane3_route_points[i].w1_for_subsequent_segment = myW1;
//RCLCPP_INFO(this->get_logger(), "carrot callback 1: route_pt %d heading = %lf deg", i, route_points[i].heading_rad*180.0/M_PI) ;
        }

        create_route_segments(lane3_route_segments, lane3_route_points, num_pts);

        have_lane3_segments = 1;    // set to true so other callback
                                    // knows we are good to go
}

void topic_callback_lane4(const nav_msgs::msg::Path::SharedPtr msg)
{
        // read in the lane1 path and create the segment structures
        int i, num_pts;
        num_pts = msg->poses.size();
        // RCLCPP_INFO(this->get_logger(), "num_pts = %d yes", num_pts) ;

        if (num_pts > MAX_POINTS || num_pts<1)
        {
                //  RCLCPP_INFO(this->get_logger(), "get_carrot callback 1: num_pts = %d", num_pts) ;
                exit (9);
        }

        num_lane4_route_segments = num_pts - 1;

        for (i=0; i < num_pts; ++i)
        {
                lane4_route_points[i].pt[0] = msg->poses[i].pose.position.x;
                lane4_route_points[i].pt[1] = msg->poses[i].pose.position.y;
                lane4_route_points[i].pt[2] = msg->poses[i].pose.position.z;
                lane4_route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z, msg->poses[i].pose.orientation.w);
                lane4_route_points[i].w1_for_subsequent_segment = myW1;
//RCLCPP_INFO(this->get_logger(), "carrot callback 1: route_pt %d heading = %lf deg", i, route_points[i].heading_rad*180.0/M_PI) ;
        }

        create_route_segments(lane4_route_segments, lane4_route_points, num_pts);

        have_lane4_segments = 1;    // set to true so other callback
                                    // knows we are good to go
}

void topic_callback_pose(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
{
        struct route_segment_struct *p_chosen_route_segments;
        int num_chosen_route_segments;

        if(!have_optimal_route_segments || !have_lane1_segments || !have_lane2_segments  || !have_lane3_segments  || !have_lane4_segments)
        {
                RCLCPP_INFO(this->get_logger(), "get_carrot: Optimal route not obtained yet.");
                return;
        }
        if(!have_route_segment_speeds)
        {
                RCLCPP_INFO(this->get_logger(), "get_carrot: Optimal route speeds not obtained yet.");
                return;
        }
        // read in the current vehicle pose and output carrot pose
        vehicle_pt[0] = msg->pose.position.x;
        vehicle_pt[1] = msg->pose.position.y;
        vehicle_pt[2] = msg->pose.position.z;
        vehicle_heading = 2.0*atan2(msg->pose.orientation.z, msg->pose.orientation.w);
        // now get the look ahead point
        look_ahead_dist = LOOK_AHEAD_DIST;

        int closest_segment_num;

        switch(chosen_route)
        {
        case 0:
                p_chosen_route_segments = optimal_route_segments;
                num_chosen_route_segments = num_optimal_route_segments;
                break;
        case 1:
                p_chosen_route_segments = lane1_route_segments;
                num_chosen_route_segments = num_lane1_route_segments;
                break;
        case 2:
                p_chosen_route_segments = lane2_route_segments;
                num_chosen_route_segments = num_lane2_route_segments;
                break;
        case 3:
                p_chosen_route_segments = lane3_route_segments;
                num_chosen_route_segments = num_lane3_route_segments;
                break;
        case 4:
                p_chosen_route_segments = lane4_route_segments;
                num_chosen_route_segments = num_lane4_route_segments;
                break;
        }

        get_look_ahead_point(look_ahead_pt,
                             &look_ahead_heading,
                             closest_pt_lines,
                             &closest_segment_num,
                             closest_pt_curve,
                             &heading_closest_pt_curve,
                             look_ahead_dist,
                             vehicle_pt,
                             num_chosen_route_segments,
                             p_chosen_route_segments);

        geometry_msgs::msg::PoseStamped output_look_ahead_pose;

        output_look_ahead_pose.pose.position.x = look_ahead_pt[0];
        output_look_ahead_pose.pose.position.y = look_ahead_pt[1];
        output_look_ahead_pose.pose.position.z = look_ahead_pt[2];
        output_look_ahead_pose.pose.orientation.x = 0.0;
        output_look_ahead_pose.pose.orientation.y = 0.0;
        output_look_ahead_pose.pose.orientation.z = sin(look_ahead_heading/2.0);
        output_look_ahead_pose.pose.orientation.w = cos(look_ahead_heading/2.0);

        output_look_ahead_pose.header.stamp = this->now();
        output_look_ahead_pose.header.frame_id = "my_frame";
        publisher_carrot_->publish(output_look_ahead_pose);

        interfaces::msg::CdcTrajectory my_trajectory;
        my_trajectory.start_x = vehicle_pt[0];
        my_trajectory.start_y = vehicle_pt[1];
        my_trajectory.start_heading = vehicle_heading;
        my_trajectory.end_x = look_ahead_pt[0];
        my_trajectory.end_y = look_ahead_pt[1];
        my_trajectory.end_heading = look_ahead_heading;
//ROS_INFO("carrot:  look_ahead_heading = %lf deg", look_ahead_heading*180.0/M_PI) ;
        my_trajectory.dd1 = 10.0;
        my_trajectory.ww1 = 1.0;
        //my_trajectory.speed = SPEED ;
        //my_trajectory.speed = optimal_route_points[closest_segment_num].speed;
        my_trajectory.speed = SPEED ;   //  FIX THIS LATER  (was 20 in videos)
        publisher_trajectory_->publish(my_trajectory);
}

rclcpp::Publisher<interfaces::msg::CdcTrajectory>::SharedPtr publisher_trajectory_;
rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher_carrot_;

rclcpp::TimerBase::SharedPtr timer_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_path_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane1_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane2_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane3_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane4_;
rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr subscription_pose_;
rclcpp::Subscription<std_msgs::msg::Float32MultiArray>::SharedPtr subscription_speeds_;
rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr publisher_cdc_text_;

visualization_msgs::msg::Marker cdc_text;

double vehicle_pt[3] = {0.0,0.0,0.0};
double vehicle_heading;
double look_ahead_pt[3] = {0.0,0.0,0.0};
double look_ahead_heading;

route_point_struct optimal_route_points[MAX_POINTS];
route_segment_struct optimal_route_segments[MAX_POINTS+1];
int num_optimal_route_segments = 0; // one less than the number of points
int have_optimal_route_segments = 0;        // initially false

route_point_struct lane1_route_points[MAX_POINTS];
route_segment_struct lane1_route_segments[MAX_POINTS+1];
int num_lane1_route_segments = 0; // one less than the number of points
int have_lane1_segments = 0;        // initially false

route_point_struct lane2_route_points[MAX_POINTS];
route_segment_struct lane2_route_segments[MAX_POINTS+1];
int num_lane2_route_segments = 0; // one less than the number of points
int have_lane2_segments = 0;       // initially false

route_point_struct lane3_route_points[MAX_POINTS];
route_segment_struct lane3_route_segments[MAX_POINTS+1];
int num_lane3_route_segments = 0; // one less than the number of points
int have_lane3_segments = 0;        // initially false

route_point_struct lane4_route_points[MAX_POINTS];
route_segment_struct lane4_route_segments[MAX_POINTS+1];
int num_lane4_route_segments = 0; // one less than the number of points
int have_lane4_segments = 0;        // initially false


int have_route_segment_speeds = 0;        // initially false

double closest_pt_lines[3];
double closest_pt_curve[3];
double heading_closest_pt_curve ;
double look_ahead_dist;

int chosen_route = 2;    // 0 is optimal path, 1 is lane 1, 2 is lane2, 3 is lane 3, 4 is lane 4

};

int main(int argc, char** argv)
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<Get_Carrot>());
        rclcpp::shutdown();
        return 0;
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
