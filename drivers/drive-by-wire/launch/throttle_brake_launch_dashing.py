from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('drive-by-wire'),
        'config',
        'params.yaml'
    )

    throttle_brake_node = Node(
            package='drive-by-wire',
            node_namespace='drive_by_wire',
            node_executable='throttle_brake_node',
            parameters=[config],
            output='screen'
        )

    ld.add_action(throttle_brake_node)

    return ld
