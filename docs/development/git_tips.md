# Git Tips - WIP

## Git Branches

``` bash
git checkout -b my_new_branch
```
This command will create a branch off of the current selected branch. Most of the time you will want to branch off of "master" so make sure master is checked out to the most up to date version before running this command.

To checkout an existing branch run:
```bash
git checkout my_existing_branch
```

If you wish to checkout a branch that is on the remote repository (Gitlab) just run command below before the `git checkout` command
```bash
git fetch
```

## Creating a commit including git add, status, clear staging area
If you have made some changes and are ready to commit some code to your local repository,
you can run below command in the root directory of the repo to stage all changes made.
```bash
git add .
```
You can use the command below to see what files have been staged. 
```bash
git status
```
If you are happy about the changes in the staging area then run the commit command below. Replace the text with whatever you want that describes the changes that were made. Git might ask you to setup your user name and user email before commiting. If so, just follow the on screen commands to complete this
```bash
git commit -m "Added tips about commiting to the git tips documentation"
```

## Pushing new branch to online repository

## Updating your local branch with new master branch
