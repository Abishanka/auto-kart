#include <chrono>
#include <memory>
#include "interfaces/msg/throttle_brake_stamped.hpp"
#include "interfaces/msg/throttle_brake_stamped_sensor.hpp"
#include "interfaces/msg/steering_pot_stamped.hpp"
#include "interfaces/msg/wheel_encoders_stamped.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"
#include "sm2335d_brake.hpp"
#include "ds_servo.hpp"
#include <fstream>
#include <iostream>
using namespace std;


using std::placeholders::_1;
using namespace std::chrono_literals;

class ThrottleBrake : public rclcpp::Node
{
public:
  ThrottleBrake()
  : Node("throttle_brake_driver")
  {

    // These are default parameters for ports. Any changes made in config parameters will override these
    brake_port_ =  this->declare_parameter<std::string>("brake_port", "/dev/ttyUSB1");
    brake_baudrate_ = this->declare_parameter<int>("brake_baudrate", 38400);
    brake_timeout_ = this->declare_parameter<int>("brake_timeout", 20);
    
    throttle_port_ = this->declare_parameter<std::string>("throttle_port", "/dev/ttyACM0");
    throttle_baudrate_ = this->declare_parameter<int>("throttle_baudrate", 115200);
    throttle_timeout_ = this->declare_parameter<int>("throttle_timeout", 20);

    //Subscribes to topic that takes in a value for throttle/brake system
    subscription_ = this->create_subscription<interfaces::msg::ThrottleBrakeStamped>(
      "/throttle_brake_effort", 10, std::bind(&ThrottleBrake::topic_callback, this, _1));

    //Publishes the sensor data
    publisherTBS_ = this->create_publisher<interfaces::msg::ThrottleBrakeStampedSensor>("/throttle_brake_stamped_sensor", 10);
    publisherWES_ = this->create_publisher<interfaces::msg::WheelEncodersStamped>("/wheel_encoders_stamped_sensor", 10);
    publisherSPS_ = this->create_publisher<interfaces::msg::SteeringPotStamped>("/steering_pot_stamped_sensor", 10);

    //timer function that checks for new inputs or sensor outputs
    timer_ = create_wall_timer(
      1ms, std::bind(&ThrottleBrake::timer_callback, this)); 
    
    // Create serial port objects
    brake_sm = std::make_unique<BrakeSmartMotorDriver>(brake_port_.c_str(), brake_baudrate_, brake_timeout_);
    throttle_servo = std::make_unique<DSServoDriver>(throttle_port_.c_str(), throttle_baudrate_, throttle_timeout_);

    //Set start up variables
    first_call = true;

    //Setup file for data
    //dataFile.open("data.csv", std::ofstream::out | std::ofstream::trunc);
    //dataFile << "Time,Accelerator_Pedal,Brake_Pressure,Back_Wheel_Encoder,Steering_Pot_Val\n";
  }

  ~ThrottleBrake()
  {
    brake_sm->updateEffort(0);
    throttle_servo->updateEffort(0);

    brake_sm->close();
    throttle_servo->close();
    RCLCPP_INFO(this->get_logger(), "Node Shutdown");
  }

private:
  void topic_callback(const interfaces::msg::ThrottleBrakeStamped::SharedPtr msg)
  {
    // Get the command and store it and toggle a flag.
    // RCLCPP_INFO(this->get_logger(), "I heard: '%f'", msg->propulsive_effort);
    if(msg->propulsive_effort <= 1 && msg->propulsive_effort >= -1){ 
	    controlCommand = msg->propulsive_effort; messageReceived = true; 
    }
  }

  void timer_callback()
  {

    if(first_call)
    {
      //on boot, keep brake at max and throttle at min
      brake_sm->updateEffort(1);
      throttle_servo->updateEffort(0);
      first_call = false;
    }

	  
    if(messageReceived) // Are the callbacks threaded? Could not be very safe.
    {
      // Brake actuator should be in 0 position before throttle is engaged
      if(controlCommand >= 0)
      {
         brake_sm->updateEffort(0);
         throttle_servo->updateEffort(controlCommand);
      }
      // throttle actuator should be in 0 effort position before brake is engaged.
      // This might be negotiable. Don't want to be in a case where the throttle is hanging so the 
      // Brake never apply. I will probably use some short timer then actuate the brakes no matter 
      // what the throttle is doing. There is no feedback anyways from the throttle
      else if(controlCommand < 0)
      { 
        throttle_servo->updateEffort(0);
        brake_sm->updateEffort(-1*controlCommand);
      }

      messageReceived = false;
    }

    // Run each actuator consequtively
    brake_sm->update();
    throttle_servo->update();

    //Obtain Sensor Data
    sensor_data = throttle_servo->getData();

    //Parse and publish sensor data
    auto messageTBS = interfaces::msg::ThrottleBrakeStampedSensor();
    messageTBS.header.stamp = get_clock()->now();
    messageTBS.accelerator_pedal = sensor_data[3];
    messageTBS.brake_pressure = sensor_data[0];
    publisherTBS_->publish(messageTBS);

    auto messageWES = interfaces::msg::WheelEncodersStamped();
    messageWES.header.stamp = get_clock()->now();
    messageWES.back_wheel = sensor_data[1];
    publisherWES_->publish(messageWES);

    auto messageSPS = interfaces::msg::SteeringPotStamped();
    messageSPS.header.stamp = get_clock()->now();
    messageSPS.steering_pot = sensor_data[2];
    publisherSPS_->publish(messageSPS);
    
    //Write data to a file for evauluation purposes
    //dataFile << messageTBS.header.stamp.sec << "." << messageTBS.header.stamp.nanosec;
    //dataFile << "," << messageTBS.accelerator_pedal;
    //dataFile << "," << messageTBS.brake_pressure;
    //dataFile << "," << messageWES.back_wheel;
    //dataFile << "," << messageSPS.steering_pot << "\n";
  }

  // -------------- Variables ---------------
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Subscription<interfaces::msg::ThrottleBrakeStamped>::SharedPtr subscription_;
  
  //Publishers for sensor data
  rclcpp::Publisher<interfaces::msg::ThrottleBrakeStampedSensor>::SharedPtr publisherTBS_;
  rclcpp::Publisher<interfaces::msg::WheelEncodersStamped>::SharedPtr publisherWES_;
  rclcpp::Publisher<interfaces::msg::SteeringPotStamped>::SharedPtr publisherSPS_;

  bool messageReceived = false;
  float controlCommand = 0.0;
  double* sensor_data;
  bool first_call;
  ofstream dataFile;

  // --- Parameters ---
  std::string brake_port_;
  int brake_baudrate_;
  int brake_timeout_;

  std::string throttle_port_;
  int throttle_baudrate_;
  int throttle_timeout_;

  // --- Drivers ---
  std::unique_ptr<BrakeSmartMotorDriver> brake_sm;
  std::unique_ptr<DSServoDriver> throttle_servo;
  
};

int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ThrottleBrake>());
  rclcpp::shutdown();
  return 0;
}
