from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('command'),
        'config',
        'params.yaml'
    )

    sylphase_node = Node(
        package='sylphase_ros2_driver',
        node_executable='solution_ros2_bridge.py',
        output='screen',
        parameters=[ {"child_frame_id": "ins"} ]
    )

    static_tf_node = Node(
        package='tf2_ros',
        node_executable='static_transform_publisher',
        name='sylphase_tf',
        arguments=["0", "0", "0", "0", "0", "0", "base_link", "ins"]
    )

    ld.add_action(sylphase_node)
    ld.add_action(static_tf_node)

    return ld
