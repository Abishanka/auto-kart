Software- WIP
=============

.. toctree::
   :maxdepth: 1
   :caption: Elements of Autonomony

   Control <control>
   Localization <localization>
   Perception <perception>
   Planning <planning.rst>
   Simulation <simulation.rst>