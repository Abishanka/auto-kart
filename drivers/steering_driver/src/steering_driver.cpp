#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"
#include "interfaces/msg/steering_stamped.hpp"
#include <unistd.h>
#include <cmath>
#include "serial/serial.h"

class SteeringDriver : public rclcpp::Node
{
  public:
    SteeringDriver() : Node("steering_driver")
    {
    }

    SteeringDriver(serial::Serial* sp) : Node("steering_driver")
    {
        subscription_ = this->create_subscription<interfaces::msg::SteeringStamped>("steering_wheel_angle", 1, std::bind(&SteeringDriver::steering_callback, this, std::placeholders::_1));
        serial_port = sp;

	//Initial RUN command to smart motor
	serial_port->write("RUN\r");
    }

  private:
    void steering_callback(const interfaces::msg::SteeringStamped::SharedPtr msg) const
    {
        if(msg->steering_wheel_angle <= 25 && msg->steering_wheel_angle >= -25.0)
        { 
          double CPR = 4000.0; //counts per rev
          double gear_ratio = 40.0; //gear ratio
          double total_ratio = CPR * gear_ratio / 360;
          int p = round(msg->steering_wheel_angle * total_ratio); //counts
          const std::string count_value = "p=" + std::to_string(p) + "\r";

          serial_port->write(count_value);
          // RCLCPP_WARN(this->get_logger(), count_value);
      	} 
    }

    serial::Serial* serial_port;
    rclcpp::Subscription<interfaces::msg::SteeringStamped>::SharedPtr subscription_;
};

int main(int argc, char * argv[])
{
  const std::string &port = "/dev/ttyUSB0";
  int baudrate = 38400;
  serial::Serial my_serial(port, baudrate, serial::Timeout::simpleTimeout(1000));

  if(my_serial.isOpen()){
    rclcpp::init(argc, argv);
    auto node = std::make_shared<SteeringDriver>(&my_serial);
    rclcpp::spin(node);
    rclcpp::shutdown();
  }

  my_serial.close();
  return 0;
}

