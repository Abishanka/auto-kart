# Team Gator Double Dragon
[![Documentation Status](https://readthedocs.org/projects/auto-kart/badge/?version=latest)](https://auto-kart.readthedocs.io/en/latest/?badge=latest)

## Documentation
Visit the [documentation](https://auto-kart.readthedocs.io/en/latest/) for more information about the team and software.
