#include <chrono>
#include <vector>
#include <math.h>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_ros/transform_broadcaster.h"

#include "interfaces/msg/radius_speed.hpp"

#include "cimar/support.h"
#include "cimar/support.cpp"

#include "cimar/route.h"
#include "cimar/route.cpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

class VehicleSimulator : public rclcpp::Node
{
public:
VehicleSimulator() : Node("vehicle_simulator")
{
        publisher_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("current_pose", 10);

        pub_timer_ = this->create_wall_timer(
                100ms, std::bind(&VehicleSimulator::pub_timer_callback, this));

        subscription_ = this->create_subscription<interfaces::msg::RadiusSpeed>("radius_speed", 10, std::bind(&VehicleSimulator::topic_callback, this, _1));

        tf2_br_ = std::make_shared<tf2_ros::TransformBroadcaster>(this);

}

void update_vehicle_pose(double new_pos[3], double *new_heading_rad,
                         double current_pos[3], double current_heading_rad,
                         int direction, double center_pt[3], double speed);
//double vehicle_pt[3] = {10.0, -20.0, 0.0} ;  // initial vehicle position
double vehicle_pt[3] = {-305.471, -1744.703, 0.0};         // initial vehicle position
//double vehicle_heading = -20*M_PI/180.0 ;    // initial vehicle heading
double vehicle_heading = 4.729808;           // initial vehicle heading
double vehicle_radius = 0.0;                       // initial turn radius
double vehicle_speed  = 0.0;                       // initial vehicle speed

private:

//void topic_callback(const interfaces::msg::RadiusSpeed::SharedPtr msg) const
void topic_callback(const interfaces::msg::RadiusSpeed::SharedPtr msg)
{
        // Assume that the twist is specified in the vehicle coordinate
        // system.  X is forward, Z is up. (not JAUS)

        // Will take twist pose.x and orientation.z and divide by 10.

        // RCLCPP_INFO(this->get_logger(), "new cmd rcvd: speed = %lf  radius = %lf", msg->speed, msg->radius) ;

        vehicle_radius = msg->radius; // store the new radius so publisher has it also
        vehicle_speed  = msg->speed;

        // RCLCPP_INFO(this->get_logger(), "new cmd rcvd: speed = %lf  radius = %lf", vehicle_speed, vehicle_radius) ;
}

void pub_timer_callback()       // call this at steady rate to publish current vehicle pose
{
        double new_pos[3], new_heading, center_pt[3], rear_axle_dir;
        int direction;

        rear_axle_dir = vehicle_heading + M_PI/2.0;
        center_pt[0] = vehicle_pt[0] + vehicle_radius*cos(rear_axle_dir);
        center_pt[1] = vehicle_pt[1] + vehicle_radius*sin(rear_axle_dir);
        center_pt[2] = vehicle_pt[2];

        direction = cdc_sgn(vehicle_radius); // positive is now a left hand turn

        update_vehicle_pose(new_pos, &new_heading, vehicle_pt, vehicle_heading,
                            direction, center_pt, vehicle_speed);

        vehicle_pt[0] = new_pos[0];
        vehicle_pt[1] = new_pos[1];
        vehicle_pt[2] = new_pos[2];
        vehicle_heading = new_heading;

        geometry_msgs::msg::PoseStamped current_vehicle_pose;
        current_vehicle_pose.pose.position.x = vehicle_pt[0];
        current_vehicle_pose.pose.position.y = vehicle_pt[1];
        current_vehicle_pose.pose.position.z = vehicle_pt[2];
        current_vehicle_pose.pose.orientation.x = 0.0;
        current_vehicle_pose.pose.orientation.y = 0.0;
        current_vehicle_pose.pose.orientation.z = sin(vehicle_heading/2.0);
        current_vehicle_pose.pose.orientation.w = cos(vehicle_heading/2.0);
//     RCLCPP_INFO(this->get_logger(),"speed = %lf  rad = %lf  cur_h = %lf  pos = %lf %lf", vehicle_speed, vehicle_radius, vehicle_heading*180.0/M_PI, vehicle_pt[0], vehicle_pt[1]) ;

        current_vehicle_pose.header.stamp = this->now();
        current_vehicle_pose.header.frame_id = "my_frame";

        publisher_->publish(current_vehicle_pose);

        // now set up the tf
        geometry_msgs::msg::TransformStamped current_T;
        current_T.child_frame_id = "vehicle";
        current_T.header.frame_id = "my_frame";
        current_T.header.stamp = this->now();

        current_T.transform.translation.x = vehicle_pt[0];
        current_T.transform.translation.y = vehicle_pt[1];
        current_T.transform.translation.z = vehicle_pt[2];

        current_T.transform.rotation.w = cos(vehicle_heading/2.0);
        current_T.transform.rotation.x = 0.0;
        current_T.transform.rotation.y = 0.0;
        current_T.transform.rotation.z = sin(vehicle_heading/2.0);

        tf2_br_->sendTransform(current_T);
}

rclcpp::TimerBase::SharedPtr pub_timer_;
rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher_;

rclcpp::Subscription<interfaces::msg::RadiusSpeed>::SharedPtr subscription_;
std::shared_ptr<tf2_ros::TransformBroadcaster> tf2_br_;

};

int main(int argc, char** argv)
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<VehicleSimulator>());
        rclcpp::shutdown();
        return 0;
}

////////////////////////////////////////////////////////////////////////
void VehicleSimulator::update_vehicle_pose(double new_pos[3], double *new_heading_rad,
                                           double current_pos[3], double current_heading_rad, int direction, double center_pt[3], double speed)
{
        // direction - 1 is left, 0 is straight, -1 is right

        int j;
        double Svec[3], radius, rotation_ang_rad, temp[3];

        if (direction == 0) // go straight
        {
                Svec[0] = cos(current_heading_rad); // unit vector along direction of travel
                Svec[1] = sin(current_heading_rad);
                Svec[2] = 0.0;
                for (j=0; j<3; ++j)
                {
                        new_pos[j] = current_pos[j] + speed*Svec[j];
                }
                *new_heading_rad = current_heading_rad;
        }

        else
        {
                for (j=0; j<3; ++j)
                {
                        Svec[j] = current_pos[j] - center_pt[j]; // vector from center to current pos
                }

                radius = cdc_mag(Svec);
                rotation_ang_rad = speed/radius; // speed represents dist to travel for this time step
                *new_heading_rad = current_heading_rad + direction*rotation_ang_rad;

                if (direction == 1) // left turn
                {
                        temp[0] = Svec[0]*cos(rotation_ang_rad) - Svec[1]*sin(rotation_ang_rad);
                        temp[1] = Svec[0]*sin(rotation_ang_rad) + Svec[1]*cos(rotation_ang_rad);
                        temp[2] = 0.0;

                        for(j=0; j<3; ++j)
                        {
                                new_pos[j] = center_pt[j] + temp[j];
                        }
                }
                else if (direction == -1) // right turn
                {
                        temp[0] = Svec[0]*cos(-rotation_ang_rad) - Svec[1]*sin(-rotation_ang_rad);
                        temp[1] = Svec[0]*sin(-rotation_ang_rad) + Svec[1]*cos(-rotation_ang_rad);
                        temp[2] = 0.0;

                        for(j=0; j<3; ++j)
                        {
                                new_pos[j] = center_pt[j] + temp[j];
                        }
                }


        }

}
/////////////////////////////////////////////////////////////////
