from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    ld = LaunchDescription()

    config = os.path.join(
        get_package_share_directory('gokart_teleop'),
        'config',
        'params.yaml'
    )

    gokart_teleop_node = Node(
            package='gokart_teleop',
            node_executable='gokart_teleop_node',
            output='screen',
            parameters = [config]
    )

    joy_node = Node(
        package='joy',
        node_executable='joy_node',
        parameters = [
            {'autorepeat_rate': 10.0}
        ]
    )

    ld.add_action(gokart_teleop_node)
    ld.add_action(joy_node)

    return ld
