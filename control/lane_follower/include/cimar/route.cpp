#include "route.h"
#include "support.h"

void create_route_segments(struct route_segment_struct route_segments[], struct route_point_struct route_points[], int num_points)
{
	// inputs -
	//      route_points:  an array of route_point_struct (point on route, heading at that point, w1 for subsequent segment)
	//      num_pts: number of points on route + 1 (the first point is repeated as the last point)
	// outputs -
	//      route_segments:  an array of route_segment_struct (p0, p1, p2, w1, length, is_straight)
	//                       Note that the number of route_segments is one less than the number of points.


	int i, j, ok ;
	double S1[3], SOL1[3], S2[3], SOL2[3], temp[3], cdc_length, u, current_pt[3], last_pt[3], denom ;
	
	for (i=0 ; i<num_points-1 ; ++i)
	{
		for (j=0 ; j<3 ; ++j)
		{
			route_segments[i].p0[j] = route_points[i].pt[j] ;
			route_segments[i].p2[j] = route_points[(i+1)%num_points].pt[j] ;
		}
		
		route_segments[i].w1 = route_points[i].w1_for_subsequent_segment ;
		
		// now need to get p1 point for the segment
		// this will be the intersection point of the lines that are tangent to the segment end points
		
		S1[0] = cos(route_points[i].heading_rad) ;
		S1[1] = sin(route_points[i].heading_rad) ;
		S1[2] = 0.0 ;
		cdc_cross(SOL1, route_points[i].pt, S1) ;
		
		S2[0] = cos(route_points[(i+1)%num_points].heading_rad) ;
		S2[1] = sin(route_points[(i+1)%num_points].heading_rad) ;
		S2[2] = 0.0 ;
		cdc_cross(SOL2, route_points[(i+1)%num_points].pt, S2) ;
		
		ok = cdc_get_intersection_of_two_lines(route_segments[i].p1, S1, SOL1, S2, SOL2) ;
		
		if(ok==999)
		{
			// the lines were parallel
			for (j=0 ; j<3 ; ++j)
			{
				temp[j] = route_segments[i].p2[j] - route_segments[i].p0[j] ;
			}
			
			for (j=0 ; j<3 ; ++j)
			{
				route_segments[i].p1[j] = route_segments[i].p0[j] + 0.5*temp[j] ;
			}
			route_segments[i].is_straight_seg = TRUE ;
		}
		else
			route_segments[i].is_straight_seg = FALSE ;
		
		cdc_length = 0.0 ;
		for (j=0 ; j<3 ; ++j)
			last_pt[j] = route_segments[i].p0[j] ;
		
		for (u=0.0 ; u<=1.00000001 ; u+=0.001)
		{
			for(j=0 ; j<3 ; ++j)
			{
				denom = (1-u)*(1-u) + 2*u*(1-u)*route_segments[i].w1 + u*u ;
				current_pt[j] = ((1-u)*(1-u)*route_segments[i].p0[j] + 2.0*u*(1-u)*route_segments[i].w1*route_segments[i].p1[j] + u*u*route_segments[i].p2[j])/denom ;
				temp[j] = current_pt[j] - last_pt[j] ;
			}
			cdc_length += cdc_mag(temp) ;
			for (j=0 ; j<3 ; ++j)
				last_pt[j] = current_pt[j] ;
		}
		route_segments[i].length = cdc_length ;
		
	}
	
}
////////////////////////////////////////////////////////////////
void get_pt_on_route(double pt[3], struct route_segment_struct *route_segment, double u)
{
	// inputs -
	//      route_segment: a pointer to a struct route_segment
	//      u: the value of u along the route segment, 0 <= u <= 1
	// outputs -
	//      pt: the coordinates of the point on the path
	
	double denom ;
	int j ;
	
	denom = (1-u)*(1-u) + 2*u*(1-u)*route_segment->w1 + u*u ;
	for(j=0 ; j<3 ; ++j)
	{
		pt[j] = ((1-u)*(1-u)*route_segment->p0[j] + 2.0*u*(1-u)*route_segment->w1*route_segment->p1[j] + u*u*route_segment->p2[j])/denom ;
	}
}
////////////////////////////////////////////////////////////////
double get_route_length_to_u(struct route_segment_struct *route_segment, double u)
{
	// inputs -
	//     route_segment: pointer to a route_segment_struct
	//     u: the value of u on the route segment, 0 <= u <= 1
	// return - distance from point p0 of segment to the point designated by u
	
	double uval, length = 0.0, current_pt[3], last_pt[3], denom, temp[3] ;
	int j ;
	
	for (j=0 ; j<3 ; ++j)
		last_pt[j] = route_segment->p0[j] ;
	
	for(uval = 0.0 ; uval <= u ; uval += 0.001)
	{
		denom = (1-uval)*(1-uval) + 2*uval*(1-uval)*route_segment->w1 + uval*uval ;
		for(j=0 ; j<3 ; ++j)
			{
				current_pt[j] = ((1-uval)*(1-uval)*route_segment->p0[j] + 2.0*uval*(1-uval)*route_segment->w1*route_segment->p1[j] + uval*uval*route_segment->p2[j])/denom ;
				temp[j] = current_pt[j] - last_pt[j] ;
			}
			length += cdc_mag(temp) ;
			for (j=0 ; j<3 ; ++j)
				last_pt[j] = current_pt[j] ;
	}
	return (length) ;
	
}
////////////////////////////////////////////////////////////////
double get_uval_for_dist(struct route_segment_struct *route_segment, double dist)
{
	// inputs -
	//     route_segment: pointer to a route_segment_struct
	//     dist: a distance along the path from the point p0
	// return - value of u that corresponds to the point that is dist away from p0
	
	void get_pt_on_route(double pt[3], struct route_segment_struct *route_segment, double u) ;
	double mydist = 0.0, last_pt[3], this_pt[3], Svec[3] ;
	int j ;
	double uval = 0.0 ;
	
	for (j=0 ; j< 3 ; ++j)
	{
		last_pt[j] = route_segment->p0[j] ;
	}
	
	
	while (mydist < dist)
	{
		uval += 0.001 ;
		get_pt_on_route(this_pt, route_segment, uval) ;
		for (j=0 ; j<3 ; ++j)
		{
			Svec[j] = this_pt[j] - last_pt[j] ;
		}
		mydist += cdc_mag(Svec) ;
		
		for (j=0 ; j<3 ; ++j)
		{
			last_pt[j] = this_pt[j] ;
		}
	}
	
	return (uval) ;
}
////////////////////////////////////////////////////////////////
void get_slope_and_curvature_on_route(double *dxdu, double *dydu, double *radius, struct route_segment_struct *segment, double u)
{
	// inputs -
	//    segment: pointer to a route_segment_struct
	//    u: value of u corresponding to a certain point on the segment, 0 <= u <= 1
	// outputs -
	//    dxdu: pointer to derivative of x with respect to u at point on segment designated by value of u
	//    dydu: pointer to derivative of y with respect to u at point on segment designated by value of u
	//    radius: radius of curvature at point on segment designated by value of u
	
	double p0x, p0y, p1x, p1y, p2x, p2y, w1 ;
	double dx, dy, dx2, dy2, denom ;
	
	p0x = segment->p0[0] ;
	p0y = segment->p0[1] ;
	p1x = segment->p1[0] ;
	p1y = segment->p1[1] ;
	p2x = segment->p2[0] ;
	p2y = segment->p2[1] ;
	w1  = segment->w1 ;

	//RCLCPP_INFO("get slope: p0 = %lf %lf, p1 = %lf %lf, p2 = %lf %lf, w1 = %lf", p0x, p0y, p1x, p1y, p2x, p2y, w1) ;
	
	denom = (1-u)*(1-u) + 2.0*(1-u)*u*segment->w1 + u*u ;
	dx = (-2 * (p0x - p2x) * (w1 - 1) * u * u + (4 * p0x * w1 - 2 * p0x - 4 * p1x + 2 * p2x) * u - 2 * p0x * w1 + 2 * p1x) / (denom*denom) ;
	dy = (-2 * (p0y - p2y) * (w1 - 1) * u * u + (4 * p0y * w1 - 2 * p0y - 4 * p1y + 2 * p2y) * u - 2 * p0y * w1 + 2 * p1y) / (denom*denom) ;

	dx2 = (-4 * (p0x - p2x) * (w1 - 1) * u + 4 * p0x * w1 - 2 * p0x - 4 * p1x + 2 * p2x) * (int) pow((double) (2 * u * u * w1 - 2 * u * u - 2 * u * w1 + 2 * u - 1), (double) (-2)) - 2 * (-2 * (p0x - p2x) * (w1 - 1) * u * u + (4 * p0x * w1 - 2 * p0x - 4 * p1x + 2 * p2x) * u - 2 * p0x * w1 + 2 * p1x) * (int) pow((double) (2 * u * u * w1 - 2 * u * u - 2 * u * w1 + 2 * u - 1), (double) (-3)) * (4 * u * w1 - 4 * u - 2 * w1 + 2);
	dy2 = (-4 * (p0y - p2y) * (w1 - 1) * u + 4 * p0y * w1 - 2 * p0y - 4 * p1y + 2 * p2y) * (int) pow((double) (2 * u * u * w1 - 2 * u * u - 2 * u * w1 + 2 * u - 1), (double) (-2)) - 2 * (-2 * (p0y - p2y) * (w1 - 1) * u * u + (4 * p0y * w1 - 2 * p0y - 4 * p1y + 2 * p2y) * u - 2 * p0y * w1 + 2 * p1y) * (int) pow((double) (2 * u * u * w1 - 2 * u * u - 2 * u * w1 + 2 * u - 1), (double) (-3)) * (4 * u * w1 - 4 * u - 2 * w1 + 2);

	*dxdu = dx ;
	*dydu = dy ;
	
	*radius = (pow(dx*dx + dy*dy, 1.5)) / (dx*dy2 - dy*dx2) ;
	
}
////////////////////////////////////////////////////////////////
