// prototypes of support functions

#ifndef SUPPORT_H
#define SUPPORT_H

#include <math.h>

// used to convert from degrees to radians
#define D2R (M_PI/180.0)

// used to convert from radians to degrees
#define R2D (180.0/M_PI)

#define TRUE 1
#define FALSE 0

double cdc_dot(double vec1[3], double vec2[3]) ;
       // returns the scalar product of 'vec1' dot 'vec2'
	   
void   cdc_cross(double ansvec[3], double vec1[3], double vec2[3]) ;
       // the cross product of 'vec1' cross 'vec2' is placed in 'ansvec'
	   
double cdc_mag(double vec[3]) ;
       // returns the magnitude of the vector 'vec'
	   
int    cdc_sgn(double v) ;
       // returns +1 if 'v' is positive, -1 if 'v' is negative, and 0 if 'v' is exactly zero
	   
int    cdc_value_near(double val, double goal, double tol) ;
       // returns 1 if 'val' is within plus or minus 'tol' of 'goal'
	   
int    cdc_get_intersection_of_two_lines(double int_pt[3], double S1[3], double SOL1[3], double S2[3], double SOL2[3]) ;
       // inputs -
        	//       S1, SOL1 - coordinates of line 1  (note that S1 must be perpendicular to SOL1)
        	//       S2, SOL2 - coordinates of line 2  (same condition as for line 1)
       // outputs -
        	//       int_pt   - the point of intersection ; garbage value if the lines do not intersect or are parallel
       // returns 1 if lines do intersect, 0 if no intersection, and 999 if they intersect at infinity (parallel lines)
	

#endif
