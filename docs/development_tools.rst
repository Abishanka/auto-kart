Development Tools
=================

Slack
-----
Join the slack to keep updated on the developement at `uf-cimar <https://uf-cimar.slack.com>`_.
Any valid @ufl.edu email will be able to create an account.

GitLab
------
Documentation and all work (except CAD) will go in `NaviGator AGK <https://gitlab.com/navigator-agk>`_
group.

Grabcad
-------
We use GrabCAD Workbench to manage mechanical design files.

Software Design
---------------
Any IDE can be used. `Visual Studio Code <https://code.visualstudio.com/>`_ is a great editor with lots of customization.

Mechanical Design
-----------------
We use SolidWorks as the primary CAD software for this project.