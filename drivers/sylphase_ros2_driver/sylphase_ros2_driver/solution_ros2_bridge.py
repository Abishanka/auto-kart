#!/usr/bin/python3

import copy
import time
import traceback
import socket
import json
import numpy

import rclpy
import tf2_ros

from std_msgs.msg import Header
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose, PoseWithCovariance, Twist, TwistWithCovariance, Point, Vector3, Quaternion, TransformStamped, Transform, Vector3Stamped

rclpy.init()

node = rclpy.create_node('solution_ros2_bridge')

# Parameters
node.declare_parameter(name="host", value="127.0.0.1")
node.declare_parameter(name="port", value=1234)
node.declare_parameter(name="child_frame_id", value="base_link")
node.declare_parameter(name="decimation", value=1)
node.declare_parameter(name="force_z_to_zero", value=False)

host = node.get_parameter(name="host").get_parameter_value().string_value
port = node.get_parameter(name="port").get_parameter_value().integer_value
child_frame_id = node.get_parameter(name="child_frame_id").get_parameter_value().string_value
decimation = node.get_parameter(name="decimation").get_parameter_value().integer_value
force_z_to_zero = node.get_parameter("force_z_to_zero").get_parameter_value().bool_value

odom_pub = node.create_publisher(Odometry, 'odom', 100)
absodom_pub = node.create_publisher(Odometry, 'absodom', 100)
acceleration_pub = node.create_publisher(Vector3Stamped, 'acceleration', 100)
broadcaster = tf2_ros.transform_broadcaster.TransformBroadcaster(node)


def handle_solution(t, d):
    d = copy.deepcopy(d)

    if force_z_to_zero and 'relative_position_enu' in d:
        d['relative_position_enu'][2] = 0

    if 'velocity_body' in d and 'angular_velocity_body' in d:
        vector_keys = ['x', 'y', 'z']  # __init__ has changed so init args need to be dictionaries

        twist = TwistWithCovariance(
            twist=Twist(
                linear=Vector3(**dict(zip(vector_keys, d['velocity_body']))),
                angular=Vector3(**dict(zip(vector_keys, d['angular_velocity_body']))),
            ),
            covariance=numpy.vstack((
                numpy.hstack((d.get('X_velocity_body_covariance', -numpy.eye(3)), numpy.zeros((3, 3)))),
                numpy.hstack((numpy.zeros((3, 3)), d.get('X_angular_velocity_body_covariance', -numpy.eye(3)))),
            )).flatten(),
        )

        ecef_cov = numpy.array(d.get('X_position_relative_position_orientation_ecef_covariance', -numpy.eye(9)))
        if 'position_ecef' in d and 'orientation_ecef' in d:
            absodom_pub.publish(Odometry(
                header=Header(
                    stamp=t,
                    frame_id='ecef',
                ),
                child_frame_id=child_frame_id,
                pose=PoseWithCovariance(
                    pose=Pose(
                        position=Point(**dict(zip(vector_keys, d['position_ecef']))),
                        orientation=Quaternion(**d['orientation_ecef']),
                    ),
                    covariance=numpy.vstack((
                        numpy.hstack((ecef_cov[0:3, 0:3], ecef_cov[0:3, 6:9])),
                        numpy.hstack((ecef_cov[6:9, 0:3], ecef_cov[6:9, 6:9])),
                    )).flatten(),
                ),
                twist=twist,
            ))
        if 'relative_position_enu' in d and 'orientation_enu' in d:
            odom_pub.publish(Odometry(
                header=Header(
                    stamp=t,
                    frame_id='enu',
                ),
                child_frame_id=child_frame_id,
                pose=PoseWithCovariance(
                    pose=Pose(
                        position=Point(**dict(zip(vector_keys, d['relative_position_enu']))),
                        orientation=Quaternion(**d['orientation_enu']),
                    ),
                    covariance=numpy.array(d.get('X_relative_position_orientation_enu_covariance', -numpy.eye(6))).flatten(),
                ),
                twist=twist,
            ))
            broadcaster.sendTransform(TransformStamped(
                header=Header(
                    stamp=t,
                    frame_id='enu',
                ),
                child_frame_id=child_frame_id,
                transform=Transform(
                    translation=Vector3(**dict(zip(vector_keys, d['relative_position_enu']))),
                    rotation=Quaternion(**d['orientation_enu']),
                )
            ))
        if 'X_acceleration_body' in d:
            acceleration_pub.publish(Vector3Stamped(
                header=Header(
                    stamp=t,
                    frame_id=child_frame_id,
                ),
                vector=Vector3(**dict(zip(vector_keys, d['X_acceleration_body']))),
            ))


def go():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, port))
    except socket.error as e:
        node.get_logger().info('Error connecting to %s port %i: %s; retrying in 3 seconds.' % (host, port, e))
        return

    first = True
    buf = ''
    count = 0
    while rclpy.ok():
        d = s.recv(2**12)
        t = node.get_clock().now().to_msg()
        if not d:
            if not first:  # if connection established messsage already printed
                node.get_logger().info('Connection to %s port %i lost; reconnecting in 3 seconds.' % (host, port))
            break
        buf += d.decode('utf-8')

        lines = buf.split('\n')
        buf = lines[-1]
        for line in lines[:-1]:
            if first:
                first = False
                node.get_logger().info('Connection to %s port %i established.' % (host, port))
                continue

            if count % decimation == 0:
                d = json.loads(line)
                handle_solution(t, d)

            count += 1


while rclpy.ok():
    try:
        go()
    except Exception:
        traceback.print_exc()
    time.sleep(3)
