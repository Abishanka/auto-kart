#include <chrono>
#include <vector>
#include <math.h>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/path.hpp"


#include "interfaces/msg/radius_speed.hpp"    // not used here yet
#include "nav_msgs/msg/path.hpp"               // input message
#include "visualization_msgs/msg/marker.hpp"

#include "cimar/support.h"
#include "cimar/support.cpp"

#include "cimar/route.h"
#include "cimar/route.cpp"

#include "cimar/pure_pursuit.h"
#include "cimar/pure_pursuit.cpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

#define MAX_POINTS 200

class DrawToRviz2 : public rclcpp::Node
{
public:
DrawToRviz2() : Node("draw_path")
{
        publisher_ = this->create_publisher<visualization_msgs::msg::Marker>("draw_optimal_route", 10);

        pub_timer_ = this->create_wall_timer(
                3000ms, std::bind(&DrawToRviz2::pub_timer_callback, this));  // publish the markers every 3 seconds

        subscription_ = this->create_subscription<nav_msgs::msg::Path>("optimal_route", 10, std::bind(&DrawToRviz2::topic_callback, this, _1));
        subscription_lane1_ = this->create_subscription<nav_msgs::msg::Path>("lane1", 10, std::bind(&DrawToRviz2::topic_callback_lane1, this, _1));
        subscription_lane2_ = this->create_subscription<nav_msgs::msg::Path>("lane2", 10, std::bind(&DrawToRviz2::topic_callback_lane2, this, _1));
        subscription_lane3_ = this->create_subscription<nav_msgs::msg::Path>("lane3", 10, std::bind(&DrawToRviz2::topic_callback_lane3, this, _1));
        subscription_lane4_ = this->create_subscription<nav_msgs::msg::Path>("lane4", 10, std::bind(&DrawToRviz2::topic_callback_lane4, this, _1));

}

private:

void topic_callback(const nav_msgs::msg::Path::SharedPtr msg)
{
        int i, j, num_poses;
        num_poses = msg->poses.size();

        if (got_optimal_path==0)
                RCLCPP_INFO(this->get_logger(), "draw_path: Got the optimal path data");
        got_optimal_path = 1;   // true

        line_strip.header.frame_id = "/my_frame";
        line_strip.header.stamp = this->now();
        line_strip.ns = "complete optimal route";
        line_strip.pose.orientation.w = 1.0;

        line_strip.id = 1;
        line_strip.type = visualization_msgs::msg::Marker::LINE_STRIP;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        line_strip.scale.x = 1.0;

        // Line strip is blue
        line_strip.color.r = 0.0;
        line_strip.color.g = 1.0;
        line_strip.color.b = 1.0;
        line_strip.color.a = 1.0;

        geometry_msgs::msg::Point p;
        struct route_point_struct route_points[MAX_POINTS];
        struct route_segment_struct route_segs[MAX_POINTS-1];
        for (i = 0; i<num_poses; ++i)     // there is one less line segment that there are points
        {
                p.x = route_points[i].pt[0] = msg->poses[i].pose.position.x;
                p.y = route_points[i].pt[1] = msg->poses[i].pose.position.y;
                p.z = route_points[i].pt[2] = msg->poses[i].pose.position.z;
                route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z,
                                                        msg->poses[i].pose.orientation.w);
                route_points[i].w1_for_subsequent_segment = 1.0;

        }

        create_route_segments(route_segs, route_points, num_poses);

        double pt[3];
        for (i=0; i<num_poses-1; ++i)
        {
                // now draw points along each of the route segments
                for(j=0; j<50; ++j)
                {
                        get_pt_on_route(pt, &(route_segs[i]), j/50.0);
                        p.x = pt[0];
                        p.y = pt[1];
                        p.z = pt[2];
                        if(!already_sent)
                                line_strip.points.push_back(p);
                }
                //p.x = route_segs[i].p0[0] ;
                //p.y = route_segs[i].p0[1] ;
                //p.z = route_segs[i].p0[2] ;
                //line_strip.points.push_back(p);
                //RCLCPP_INFO(this->get_logger(), "draw_path: p0 = %lf, %lf, %lf", route_segs[i].p0[0], route_segs[i].p0[1], route_segs[i].p0[2]);
        }

        // now 'paint' the lane boundaries
        lane_markers_list.header.frame_id = "/my_frame";
        lane_markers_list.header.stamp = this->now();
        lane_markers_list.ns = "lane boundary markers";
        lane_markers_list.pose.orientation.w = 1.0;

        lane_markers_list.id = 1;
        lane_markers_list.type = visualization_msgs::msg::Marker::LINE_LIST;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        lane_markers_list.scale.x = 1.0;

        // Line strip is blue

        lane_markers_list.color.r = 1.0;
        lane_markers_list.color.g = 1.0;
        lane_markers_list.color.b = 1.0;
        lane_markers_list.color.a = 1.0;

        //front_straight
        p.x =  -306.457;    // inside stripe
        p.y = -1178.318;
        p.z =     0.0;
        lane_markers_list.points.push_back(p);

        p.x =  -290.633;
        p.y = -2136.317;
        lane_markers_list.points.push_back(p);

        // add a small line to identify turn 1
        p.x =  -290.633;   p.y = -2136.317;   lane_markers_list.points.push_back(p);
        p.x =  -250.633;   p.y = -2136.317;   lane_markers_list.points.push_back(p);

        p.x =  -322.018;    // outside stripe
        p.y = -1178.318;
        lane_markers_list.points.push_back(p);

        p.x =  -304.457;
        p.y = -2136.317;
        lane_markers_list.points.push_back(p);

        // short chute 1-2
        p.x =    -1.709;    // inside stripe
        p.y = -2396.937;
        p.z =     0.0;
        lane_markers_list.points.push_back(p);

        p.x =   141.787;
        p.y = -2394.361;
        lane_markers_list.points.push_back(p);

        p.x =    -1.709;    // outside stripe
        p.y = -2410.702;
        lane_markers_list.points.push_back(p);

        p.x =   141.787;
        p.y = -2408.277;
        lane_markers_list.points.push_back(p);

        // back straight
        p.x =   426.307;    // inside stripe
        p.y = -2094.640;
        p.z =     0.0;
        lane_markers_list.points.push_back(p);

        p.x =   410.213;
        p.y = -1168.580;
        lane_markers_list.points.push_back(p);

        p.x =   440.120;    // outside stripe
        p.y = -2094.640;
        lane_markers_list.points.push_back(p);

        p.x =   424.026;
        p.y = -1168.580;
        lane_markers_list.points.push_back(p);

        // short chute 3-4
        p.x =   111.342;    // inside stripe
        p.y =  -878.310;
        p.z =     0.0;
        lane_markers_list.points.push_back(p);

        p.x =   -21.850;
        p.y =  -880.583;
        lane_markers_list.points.push_back(p);

        p.x =   111.342;    // outside stripe
        p.y =  -864.437;
        lane_markers_list.points.push_back(p);

        p.x =   -21.850;
        p.y =  -866.882;
        lane_markers_list.points.push_back(p);

/*      stuff to just check if a line strip would be drawn
        float f = 0.0;
        // Create the vertices for the points and lines
        for (uint32_t i = 0; i < 100; ++i)
        {
                float y = 10 * sin(f + i / 100.0f * 2 * M_PI);
                float z = 10 * cos(f + i / 100.0f * 2 * M_PI);

                geometry_msgs::msg::Point p;
                p.x = (int32_t)i - 50;
                p.y = y;
                p.z = z;

                line_strip.points.push_back(p);
        }
 */

}

void topic_callback_lane1(const nav_msgs::msg::Path::SharedPtr msg)
{
        int i, j, num_poses;
        num_poses = msg->poses.size();

        if (got_lane1_path==0)
                RCLCPP_INFO(this->get_logger(), "draw_path: Got the lane1 path data");
        got_lane1_path = 1;   // true

        lane1_strip.header.frame_id = "/my_frame";
        lane1_strip.header.stamp = this->now();
        lane1_strip.ns = "lane1 route";
        lane1_strip.pose.orientation.w = 1.0;

        lane1_strip.id = 1;
        lane1_strip.type = visualization_msgs::msg::Marker::LINE_STRIP;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        lane1_strip.scale.x = 1.0;

        // Line strip is blue
        lane1_strip.color.r = 0.0;
        lane1_strip.color.g = 1.0;
        lane1_strip.color.b = 0.0;
        lane1_strip.color.a = 1.0;

        geometry_msgs::msg::Point p;
        struct route_point_struct route_points[MAX_POINTS];
        struct route_segment_struct route_segs[MAX_POINTS-1];
        for (i = 0; i<num_poses; ++i)
        {
                p.x = route_points[i].pt[0] = msg->poses[i].pose.position.x;
                p.y = route_points[i].pt[1] = msg->poses[i].pose.position.y;
                p.z = route_points[i].pt[2] = msg->poses[i].pose.position.z;
                route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z,
                                                        msg->poses[i].pose.orientation.w);
                route_points[i].w1_for_subsequent_segment = 1.0;

        }

        create_route_segments(route_segs, route_points, num_poses);

        double pt[3];
        for (i=0; i<num_poses-1; ++i)
        {
                // now draw points along each of the route segments
                for(j=0; j<50; ++j)
                {
                        get_pt_on_route(pt, &(route_segs[i]), j/50.0);
                        p.x = pt[0];
                        p.y = pt[1];
                        p.z = pt[2];
                        if(!already_sent)
                                lane1_strip.points.push_back(p);
                }
        }

}

void topic_callback_lane2(const nav_msgs::msg::Path::SharedPtr msg)
{
        int i, j, num_poses;
        num_poses = msg->poses.size();

        if (got_lane2_path==0)
                RCLCPP_INFO(this->get_logger(), "draw_path: Got the lane2 path data");
        got_lane2_path = 1;   // true

        lane2_strip.header.frame_id = "/my_frame";
        lane2_strip.header.stamp = this->now();
        lane2_strip.ns = "lane2 route";
        lane2_strip.pose.orientation.w = 1.0;

        lane2_strip.id = 1;
        lane2_strip.type = visualization_msgs::msg::Marker::LINE_STRIP;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        lane2_strip.scale.x = 1.0;

        // Line strip is blue
        lane2_strip.color.r = 0.0;
        lane2_strip.color.g = 1.0;
        lane2_strip.color.b = 0.0;
        lane2_strip.color.a = 1.0;

        geometry_msgs::msg::Point p;
        struct route_point_struct route_points[MAX_POINTS];
        struct route_segment_struct route_segs[MAX_POINTS-1];
        for (i = 0; i<num_poses; ++i)
        {
                p.x = route_points[i].pt[0] = msg->poses[i].pose.position.x;
                p.y = route_points[i].pt[1] = msg->poses[i].pose.position.y;
                p.z = route_points[i].pt[2] = msg->poses[i].pose.position.z;
                route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z,
                                                        msg->poses[i].pose.orientation.w);
                route_points[i].w1_for_subsequent_segment = 1.0;

        }

        create_route_segments(route_segs, route_points, num_poses);

        double pt[3];
        for (i=0; i<num_poses-1; ++i)
        {
                // now draw points along each of the route segments
                for(j=0; j<50; ++j)
                {
                        get_pt_on_route(pt, &(route_segs[i]), j/50.0);
                        p.x = pt[0];
                        p.y = pt[1];
                        p.z = pt[2];
                        if(!already_sent)
                                lane2_strip.points.push_back(p);
                }
        }

}

void topic_callback_lane3(const nav_msgs::msg::Path::SharedPtr msg)
{
        int i, j, num_poses;
        num_poses = msg->poses.size();

        if (got_lane3_path==0)
                RCLCPP_INFO(this->get_logger(), "draw_path: Got the lane3 path data");
        got_lane3_path = 1;   // true

        lane3_strip.header.frame_id = "/my_frame";
        lane3_strip.header.stamp = this->now();
        lane3_strip.ns = "lane3 route";
        lane3_strip.pose.orientation.w = 1.0;

        lane3_strip.id = 1;
        lane3_strip.type = visualization_msgs::msg::Marker::LINE_STRIP;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        lane3_strip.scale.x = 1.0;

        // Line strip is blue
        lane3_strip.color.r = 0.0;
        lane3_strip.color.g = 1.0;
        lane3_strip.color.b = 0.0;
        lane3_strip.color.a = 1.0;

        geometry_msgs::msg::Point p;
        struct route_point_struct route_points[MAX_POINTS];
        struct route_segment_struct route_segs[MAX_POINTS-1];
        for (i = 0; i<num_poses; ++i)
        {
                p.x = route_points[i].pt[0] = msg->poses[i].pose.position.x;
                p.y = route_points[i].pt[1] = msg->poses[i].pose.position.y;
                p.z = route_points[i].pt[2] = msg->poses[i].pose.position.z;
                route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z,
                                                        msg->poses[i].pose.orientation.w);
                route_points[i].w1_for_subsequent_segment = 1.0;

        }

        create_route_segments(route_segs, route_points, num_poses);

        double pt[3];
        for (i=0; i<num_poses-1; ++i)
        {
                // now draw points along each of the route segments
                for(j=0; j<50; ++j)
                {
                        get_pt_on_route(pt, &(route_segs[i]), j/50.0);
                        p.x = pt[0];
                        p.y = pt[1];
                        p.z = pt[2];
                        if(!already_sent)
                                lane3_strip.points.push_back(p);
                }
        }

}

void topic_callback_lane4(const nav_msgs::msg::Path::SharedPtr msg)
{
        int i, j, num_poses;
        num_poses = msg->poses.size();

        if (got_lane4_path==0)
                RCLCPP_INFO(this->get_logger(), "draw_path: Got the lane4 path data");
        got_lane4_path = 1;   // true

        lane4_strip.header.frame_id = "/my_frame";
        lane4_strip.header.stamp = this->now();
        lane4_strip.ns = "lane4 route";
        lane4_strip.pose.orientation.w = 1.0;

        lane4_strip.id = 1;
        lane4_strip.type = visualization_msgs::msg::Marker::LINE_STRIP;

        // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
        lane4_strip.scale.x = 1.0;

        // Line strip is blue
        lane4_strip.color.r = 0.0;
        lane4_strip.color.g = 1.0;
        lane4_strip.color.b = 0.0;
        lane4_strip.color.a = 1.0;

        geometry_msgs::msg::Point p;
        struct route_point_struct route_points[MAX_POINTS];
        struct route_segment_struct route_segs[MAX_POINTS-1];
        for (i = 0; i<num_poses; ++i)
        {
                p.x = route_points[i].pt[0] = msg->poses[i].pose.position.x;
                p.y = route_points[i].pt[1] = msg->poses[i].pose.position.y;
                p.z = route_points[i].pt[2] = msg->poses[i].pose.position.z;
                route_points[i].heading_rad = 2.0*atan2(msg->poses[i].pose.orientation.z,
                                                        msg->poses[i].pose.orientation.w);
                route_points[i].w1_for_subsequent_segment = 1.0;

        }

        create_route_segments(route_segs, route_points, num_poses);

        double pt[3];
        for (i=0; i<num_poses-1; ++i)
        {
                // now draw points along each of the route segments
                for(j=0; j<50; ++j)
                {
                        get_pt_on_route(pt, &(route_segs[i]), j/50.0);
                        p.x = pt[0];
                        p.y = pt[1];
                        p.z = pt[2];
                        if(!already_sent)
                                lane4_strip.points.push_back(p);
                }
        }

}

void pub_timer_callback()       // call this at steady rate to publish current vehicle pose
{
        if (!got_optimal_path || !got_lane1_path  || !got_lane3_path  || !got_lane4_path)
        {
                RCLCPP_INFO(this->get_logger(), "draw_path: Do not have all the path data yet");
                return;
        }

        //publisher_->publish(line_strip) ;  // optimal route
        publisher_->publish(lane1_strip) ;
        publisher_->publish(lane2_strip) ;
        publisher_->publish(lane3_strip) ;
        publisher_->publish(lane4_strip) ;
        publisher_->publish(lane_markers_list) ;
        already_sent = 1.0 ;   // true

}

rclcpp::TimerBase::SharedPtr pub_timer_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane1_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane2_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane3_;
rclcpp::Subscription<nav_msgs::msg::Path>::SharedPtr subscription_lane4_;
rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr publisher_;

visualization_msgs::msg::Marker line_strip, lane_markers_list, lane1_strip, lane2_strip, lane3_strip, lane4_strip ;

int got_optimal_path = 0;     // initialize to false
int got_lane1_path   = 0;
int got_lane2_path   = 0;
int got_lane3_path   = 0;
int got_lane4_path   = 0;
int already_sent = 0;         // initialize to false

};

int main(int argc, char** argv)
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<DrawToRviz2>());
        rclcpp::shutdown();
        return 0;
}

////////////////////////////////////////////////////////////////////////
