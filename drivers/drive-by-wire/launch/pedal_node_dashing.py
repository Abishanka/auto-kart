from launch import LaunchDescription
from launch_ros.actions import Node
import os

def generate_launch_description():
    ld = LaunchDescription()

    pedal_node = Node(
            package='drive-by-wire',
            node_executable='pedal_node',
            output='screen',
        )

    ld.add_action(pedal_node)

    return ld

