#include <math.h>
#include <stdio.h>
#include <sstream>

#include <chrono>
#include <vector>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "ament_index_cpp/get_package_share_directory.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float32_multi_array.hpp"
#include "nav_msgs/msg/path.hpp" // output message
#include "geometry_msgs/msg/pose_stamped.hpp"

#include "cimar/support.h"
#include "cimar/support.cpp"

#include "cimar/route.h"
#include "cimar/route.cpp"

using namespace std::chrono_literals;

#define MAX_POINTS 200
///////////////////////////////////
class MinimalPublisher : public rclcpp::Node
{
public:
MinimalPublisher()
        : Node("optimal_route"), count_(0)
{
        publisher_path_ = this->create_publisher<nav_msgs::msg::Path>("optimal_route", 10);
        publisher_lane1_ = this->create_publisher<nav_msgs::msg::Path>("lane1", 10);
        publisher_lane2_ = this->create_publisher<nav_msgs::msg::Path>("lane2", 10);
        publisher_lane3_ = this->create_publisher<nav_msgs::msg::Path>("lane3", 10);
        publisher_lane4_ = this->create_publisher<nav_msgs::msg::Path>("lane4", 10);
        publisher_speeds_ = this->create_publisher<std_msgs::msg::Float32MultiArray>("optimal_route_speeds", 10);

        timer_ = this->create_wall_timer(
                1000ms, std::bind(&MinimalPublisher::timer_callback, this)); // every 1 second

        load_up_optimal_data();
        load_up_lane_data();
}

void load_up_optimal_data();
void load_up_lane_data();

private:
nav_msgs::msg::Path optimal_data;
nav_msgs::msg::Path lane1_data;
nav_msgs::msg::Path lane2_data;
nav_msgs::msg::Path lane3_data;
nav_msgs::msg::Path lane4_data;
std_msgs::msg::Float32MultiArray speed_array;

void timer_callback()
{
        //RCLCPP_INFO(this->get_logger(), "Publishing: stuff") ;
        publisher_path_->publish(optimal_data);
        publisher_lane1_->publish(lane1_data);
        publisher_lane2_->publish(lane2_data);
        publisher_lane3_->publish(lane3_data);
        publisher_lane4_->publish(lane4_data);
        //publisher_path_->publish(lane4_data);
        publisher_speeds_->publish(speed_array);
}

rclcpp::TimerBase::SharedPtr timer_;
rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_path_;
rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_lane1_;
rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_lane2_;
rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_lane3_;
rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr publisher_lane4_;
rclcpp::Publisher<std_msgs::msg::Float32MultiArray>::SharedPtr publisher_speeds_;
size_t count_;

};

int main(int argc, char * argv[])
{
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<MinimalPublisher>());
        rclcpp::shutdown();
        return 0;
}
//////////////////////////////////////////////////////////////////////

void MinimalPublisher::load_up_optimal_data()
{
        // read in the data points
        route_point_struct route_points[MAX_POINTS];
        int i, j, num_points;

        FILE *fp;

        std::string package_share_directory = ament_index_cpp::get_package_share_directory("lane_follower");

        std::string route_directory = package_share_directory + "/data/route_indy.txt";

        fp = fopen(route_directory.c_str(), "r");

        if (fp == NULL)
        {
                RCLCPP_INFO(this->get_logger(), "could not open New Smyrna data file");
                exit(9);
        }
        RCLCPP_INFO(this->get_logger(), "ok, file opened just fine");

        fscanf(fp, "%d", &num_points);
        RCLCPP_INFO(this->get_logger(), "New Smyrna, num_points = %d", num_points);
        if (num_points > MAX_POINTS || num_points <1)
        {
                RCLCPP_INFO(this->get_logger(), "New Smyrna, num_points must be between 1 and %d", MAX_POINTS);
                exit(9);
        }

        geometry_msgs::msg::PoseStamped this_pose;
        optimal_data.header.stamp = this->now();
        optimal_data.header.frame_id = "my_frame";

        for (i=0; i< num_points; ++i)
        {
                for (j=0; j<2; ++j)
                        fscanf(fp, "%lf", &(route_points[i].pt[j]));
                route_points[i].pt[2] = 0.0;

                fscanf(fp, "%lf", &(route_points[i].heading_rad));
                fscanf(fp, "%lf", &(route_points[i].w1_for_subsequent_segment));
                fscanf(fp, "%lf", &(route_points[i].speed));

                this_pose.pose.position.x = route_points[i].pt[0];
                this_pose.pose.position.y = route_points[i].pt[1];
                this_pose.pose.position.z = route_points[i].pt[2];
                this_pose.pose.orientation.x = 0.0;
                this_pose.pose.orientation.y = 0.0;
                this_pose.pose.orientation.z = sin(route_points[i].heading_rad/2.0);
                this_pose.pose.orientation.w = cos(route_points[i].heading_rad/2.0);

                optimal_data.poses.push_back(this_pose);

                //set up the speed_array
                speed_array.data.push_back(route_points[i].speed);

        }
}
//////////////////////////////////////////////////////////////////////
void MinimalPublisher::load_up_lane_data()
{
        // lane stripe data

        double entry1_inner[2], entry1_outer[2];
        double exit1_inner[2],  exit1_outer[2];
        double entry2_inner[2], entry2_outer[2];
        double exit2_inner[2],  exit2_outer[2];
        double entry3_inner[2], entry3_outer[2];
        double exit3_inner[2],  exit3_outer[2];
        double entry4_inner[2], entry4_outer[2];
        double exit4_inner[2],  exit4_outer[2];

        entry1_inner[0] = -290.633;  entry1_inner[1] = -2136.317;
        entry1_outer[0] = -304.457;  entry1_outer[1] = -2136.317;
        exit1_inner[0]  =   -1.709;  exit1_inner[1]  = -2396.937;
        exit1_outer[0]  =   -1.709;  exit1_outer[1]  = -2410.702;

        entry2_inner[0] =  141.787;  entry2_inner[1] = -2394.361;
        entry2_outer[0] =  141.787;  entry2_outer[1] = -2408.277;
        exit2_inner[0]  =  426.307;  exit2_inner[1]  = -2094.640;
        exit2_outer[0]  =  440.120;  exit2_outer[1]  = -2094.640;

        entry3_inner[0] =  410.213;  entry3_inner[1] = -1168.580;
        entry3_outer[0] =  424.026;  entry3_outer[1] = -1168.580;
        exit3_inner[0]  =  111.342;  exit3_inner[1]  =  -878.310;
        exit3_outer[0]  =  111.342;  exit3_outer[1]  =  -864.437;

        entry4_inner[0] =  -21.850;  entry4_inner[1] =  -880.583;
        entry4_outer[0] =  -21.850;  entry4_outer[1] =  -866.882;
        exit4_inner[0]  = -306.457;  exit4_inner[1]  = -1178.318;
        exit4_outer[0]  = -322.018;  exit4_outer[1]  = -1178.318;

        double turn1_pt[2] ;
        double turn2_pt[2] ;
        double turn3_pt[2] ;
        double turn4_pt[2] ;

        turn1_pt[0] =  -220.787 ;
        turn1_pt[1] = -2329.070 ;
        turn2_pt[0] =   336.023 ;
        turn2_pt[1] = -2323.240 ;
        turn3_pt[0] =   350.839 ;
        turn3_pt[1] =  -969.014 ;
        turn4_pt[0] =  -234.046 ;
        turn4_pt[1] =  -955.000 ;

        geometry_msgs::msg::PoseStamped lane1_pose, lane2_pose, lane3_pose, lane4_pose;
        lane1_data.header.stamp = lane2_data.header.stamp = lane3_data.header.stamp = lane4_data.header.stamp = this->now();
        lane1_data.header.frame_id = lane2_data.header.frame_id = lane3_data.header.frame_id = lane4_data.header.frame_id = "my_frame";

        // get heading angles along the straights ;

        double heading_front_straight, heading_chute12, heading_back_straight, heading_chute34;

        heading_front_straight = atan2(entry1_outer[1]-exit4_outer[1], entry1_outer[0]-exit4_outer[0]);
        heading_chute12        = atan2(entry2_outer[1]-exit1_outer[1], entry2_outer[0]-exit1_outer[0]);
        heading_back_straight  = atan2(entry3_outer[1]-exit2_outer[1], entry3_outer[0]-exit2_outer[0]);
        heading_chute34        = atan2(entry4_outer[1]-exit3_outer[1], entry4_outer[0]-exit3_outer[0]);

        //RCLCPP_INFO(this->get_logger(), "headings in degrees = %lf, %lf, %lf, %lf", heading_front_straight*180.0/M_PI,
        //             heading_chute12*180.0/M_PI, heading_back_straight*180.0/M_PI, heading_chute34*180.0/M_PI);

        // front straight, turn 4 exit
        int i;
        double width, myvec[3] = {0.0,0.0,0.0};
        // lane 1 is width/8 from inside ;  lanes 2, 3, and 4 are then width/4 apart from one anothe

        myvec[0] = exit4_outer[0] - exit4_inner[0];
        myvec[1] = exit4_outer[1] - exit4_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = exit4_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = exit4_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane1_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = exit4_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = exit4_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane2_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = exit4_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = exit4_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane3_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = exit4_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = exit4_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane4_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // front straight, turn 1 entrance
        myvec[0] = entry1_outer[0] - entry1_inner[0];
        myvec[1] = entry1_outer[1] - entry1_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = entry1_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = entry1_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane1_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = entry1_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = entry1_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane2_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = entry1_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = entry1_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane3_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = entry1_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = entry1_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane4_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // now push the turn 1 point (will use the same for lanes 1-2 and 3-4)
        lane1_pose.pose.position.x = turn1_pt[0] ;
        lane1_pose.pose.position.y = turn1_pt[1] ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin((heading_front_straight+45.0*M_PI/180.0)/2.0);
        lane1_pose.pose.orientation.w = cos((heading_front_straight+45.0*M_PI/180.0)/2.0);
        lane1_data.poses.push_back(lane1_pose);
        lane2_data.poses.push_back(lane1_pose) ;

        double tempvec[2] ;
        tempvec[0] = cos(heading_front_straight-45.0*M_PI/180.0) ;
        tempvec[1] = sin(heading_front_straight-45.0*M_PI/180.0) ;
        lane3_pose.pose.position.x = turn1_pt[0] + 8.0 * tempvec[0] ;
        lane3_pose.pose.position.y = turn1_pt[1] + 8.0 * tempvec[1] ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin((heading_front_straight+45.0*M_PI/180.0)/2.0);
        lane3_pose.pose.orientation.w = cos((heading_front_straight+45.0*M_PI/180.0)/2.0);
        lane3_data.poses.push_back(lane3_pose) ;
        lane4_data.poses.push_back(lane3_pose) ;
        // ...........................................

        // exit turn 1 points
        myvec[0] = exit1_outer[0] - exit1_inner[0];
        myvec[1] = exit1_outer[1] - exit1_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = exit1_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = exit1_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane1_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = exit1_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = exit1_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane2_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = exit1_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = exit1_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane3_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = exit1_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = exit1_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane4_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // entry turn 2 route_points
        myvec[0] = entry2_outer[0] - entry2_inner[0];
        myvec[1] = entry2_outer[1] - entry2_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = entry2_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = entry2_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane1_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = entry2_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = entry2_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane2_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = entry2_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = entry2_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane3_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = entry2_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = entry2_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_chute12/2.0);
        lane4_pose.pose.orientation.w = cos(heading_chute12/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // now push the turn 2 point (will use the same for all lanes for now)
        lane1_pose.pose.position.x = turn2_pt[0] ;
        lane1_pose.pose.position.y = turn2_pt[1] ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin((heading_chute12+45.0*M_PI/180.0)/2.0);
        lane1_pose.pose.orientation.w = cos((heading_chute12+45.0*M_PI/180.0)/2.0);
        lane1_data.poses.push_back(lane1_pose);
        lane2_data.poses.push_back(lane1_pose) ;

        tempvec[0] = cos(heading_chute12-45.0*M_PI/180.0) ;
        tempvec[1] = sin(heading_chute12-45.0*M_PI/180.0) ;
        lane3_pose.pose.position.x = turn2_pt[0] + 8.0 * tempvec[0] ;
        lane3_pose.pose.position.y = turn2_pt[1] + 8.0 * tempvec[1] ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin((heading_chute12+45.0*M_PI/180.0)/2.0);
        lane3_pose.pose.orientation.w = cos((heading_chute12+45.0*M_PI/180.0)/2.0);
        lane3_data.poses.push_back(lane3_pose) ;
        lane4_data.poses.push_back(lane3_pose) ;

        // ...........................................

        // exit turn 2 points
        myvec[0] = exit2_outer[0] - exit2_inner[0];
        myvec[1] = exit2_outer[1] - exit2_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = exit2_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = exit2_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane1_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = exit2_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = exit2_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane2_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = exit2_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = exit2_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane3_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = exit2_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = exit2_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane4_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // enter turn 3 points
        myvec[0] = entry3_outer[0] - entry3_inner[0];
        myvec[1] = entry3_outer[1] - entry3_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = entry3_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = entry3_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane1_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = entry3_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = entry3_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane2_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = entry3_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = entry3_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane3_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = entry3_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = entry3_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_back_straight/2.0);
        lane4_pose.pose.orientation.w = cos(heading_back_straight/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // now push the turn 3 point (will use the same for all lanes for now)
        lane1_pose.pose.position.x = turn3_pt[0] ;
        lane1_pose.pose.position.y = turn3_pt[1] ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin((heading_back_straight+45.0*M_PI/180.0)/2.0);
        lane1_pose.pose.orientation.w = cos((heading_back_straight+45.0*M_PI/180.0)/2.0);
        lane1_data.poses.push_back(lane1_pose);
        lane2_data.poses.push_back(lane1_pose) ;

        tempvec[0] = cos(heading_back_straight-45.0*M_PI/180.0) ;
        tempvec[1] = sin(heading_back_straight-45.0*M_PI/180.0) ;
        lane3_pose.pose.position.x = turn3_pt[0] + 8.0 * tempvec[0] ;
        lane3_pose.pose.position.y = turn3_pt[1] + 8.0 * tempvec[1] ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin((heading_back_straight+45.0*M_PI/180.0)/2.0);
        lane3_pose.pose.orientation.w = cos((heading_back_straight+45.0*M_PI/180.0)/2.0);
        lane3_data.poses.push_back(lane3_pose) ;
        lane4_data.poses.push_back(lane3_pose) ;

        // ...........................................

        // exit turn 3 points
        myvec[0] = exit3_outer[0] - exit3_inner[0];
        myvec[1] = exit3_outer[1] - exit3_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = exit3_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = exit3_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane1_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = exit3_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = exit3_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane2_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = exit3_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = exit3_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane3_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = exit3_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = exit3_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane4_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................


        // enter turn 4 points
        myvec[0] = entry4_outer[0] - entry4_inner[0];
        myvec[1] = entry4_outer[1] - entry4_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = entry4_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = entry4_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane1_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = entry4_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = entry4_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane2_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = entry4_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = entry4_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane3_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = entry4_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = entry4_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_chute34/2.0);
        lane4_pose.pose.orientation.w = cos(heading_chute34/2.0);
        lane4_data.poses.push_back(lane4_pose);
        // ...........................................

        // now push the turn 4 point (will use the same for all lanes for now)
        lane1_pose.pose.position.x = turn4_pt[0] ;
        lane1_pose.pose.position.y = turn4_pt[1] ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin((heading_chute34+45.0*M_PI/180.0)/2.0);
        lane1_pose.pose.orientation.w = cos((heading_chute34+45.0*M_PI/180.0)/2.0);
        lane1_data.poses.push_back(lane1_pose);
        lane2_data.poses.push_back(lane1_pose) ;

        tempvec[0] = cos(heading_chute34-45.0*M_PI/180.0) ;
        tempvec[1] = sin(heading_chute34-45.0*M_PI/180.0) ;
        lane3_pose.pose.position.x = turn4_pt[0] + 8.0 * tempvec[0] ;
        lane3_pose.pose.position.y = turn4_pt[1] + 8.0 * tempvec[1] ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin((heading_chute34+45.0*M_PI/180.0)/2.0);
        lane3_pose.pose.orientation.w = cos((heading_chute34+45.0*M_PI/180.0)/2.0);
        lane3_data.poses.push_back(lane3_pose) ;
        lane4_data.poses.push_back(lane3_pose) ;
        // ...........................................

        // now repeat the first point on the path
        // exit turn 4
        myvec[0] = exit4_outer[0] - exit4_inner[0];
        myvec[1] = exit4_outer[1] - exit4_inner[1];
        width = cdc_mag(myvec);
        for (i=0; i<3; ++i)
                myvec[i] = myvec[i]/width;

        lane1_pose.pose.position.x = exit4_inner[0] + myvec[0] * width/8.0 ;
        lane1_pose.pose.position.y = exit4_inner[1] + myvec[1] * width/8.0 ;
        lane1_pose.pose.position.z = 0.0 ;
        lane1_pose.pose.orientation.x = 0.0;
        lane1_pose.pose.orientation.y = 0.0;
        lane1_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane1_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane1_data.poses.push_back(lane1_pose);

        lane2_pose.pose.position.x = exit4_inner[0] + myvec[0] * 3.0*width/8.0 ;
        lane2_pose.pose.position.y = exit4_inner[1] + myvec[1] * 3.0*width/8.0 ;
        lane2_pose.pose.position.z = 0.0 ;
        lane2_pose.pose.orientation.x = 0.0;
        lane2_pose.pose.orientation.y = 0.0;
        lane2_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane2_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane2_data.poses.push_back(lane2_pose);

        lane3_pose.pose.position.x = exit4_inner[0] + myvec[0] * 5.0*width/8.0 ;
        lane3_pose.pose.position.y = exit4_inner[1] + myvec[1] * 5.0*width/8.0 ;
        lane3_pose.pose.position.z = 0.0 ;
        lane3_pose.pose.orientation.x = 0.0;
        lane3_pose.pose.orientation.y = 0.0;
        lane3_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane3_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane3_data.poses.push_back(lane3_pose);

        lane4_pose.pose.position.x = exit4_inner[0] + myvec[0] * 7.0*width/8.0 ;
        lane4_pose.pose.position.y = exit4_inner[1] + myvec[1] * 7.0*width/8.0 ;
        lane4_pose.pose.position.z = 0.0 ;
        lane4_pose.pose.orientation.x = 0.0;
        lane4_pose.pose.orientation.y = 0.0;
        lane4_pose.pose.orientation.z = sin(heading_front_straight/2.0);
        lane4_pose.pose.orientation.w = cos(heading_front_straight/2.0);
        lane4_data.poses.push_back(lane4_pose);
}
