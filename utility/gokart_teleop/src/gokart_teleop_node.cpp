#include <functional>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/joy.hpp"
#include "interfaces/msg/steering_stamped.hpp"
#include "interfaces/msg/throttle_brake_stamped.hpp"

/*
	Converts Joy messages from Xbox 360 Controller to VehicleCmd
	Available data from Xbox 360 Controller using Joy
	--------------------------------------------------
	Axes:
		axes[0]: Left Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
		axes[1]: Left Thumbstick-Vertical,	Range: [1,-1], 0 Centered, 1 Up
		axes[2]: Left Trigger,				Range: [1,-1], 1 when unpressed
		axes[3]: Right Thumbstick-Horizontal,Range: [1,-1], 0 Centered, 1 Left
		axes[4]: Right Thumbstick-Vertical, Range: [1,-1], 0 Centered, 1 Up
		axes[5]: Right Trigger,				Range: [1,-1], 1 when unpressed
		axes[6]: Horizontal D-Pad			Vales: [1], [0], [-1], 1 pressed left, -1 pressed right, 0 not pressed
		axes[7]: Verical D-Pad				Vales: [1], [0], [-1], 1 pressed up, -1 pressed down, 0 not pressed
	Buttons:
		buttons[0]: "A",					Values: [0], [1], 1 when pressed
		buttons[1]: "B",					Values: [0], [1], 1 when pressed
		buttons[2]: "X",					Values: [0], [1], 1 when pressed
		buttons[3]: "Y",					Values: [0], [1], 1 when pressed
		buttons[4]: Left Bumper,			Values: [0], [1], 1 when pressed
		buttons[5]: Right Bumper,			Values: [0], [1], 1 when pressed
		buttons[6]: "Back" Button,			Values: [0], [1], 1 when pressed
		buttons[7]: "Start" Button,			Values: [0], [1], 1 when pressed
		buttons[8]: Xbox Button,			Values: [0], [1], 1 when pressed
		buttons[9]: Left Thumbstick Press,  Values: [0], [1], 1 when pressed
		buttons[10]: Right Thumbstick Press,Values: [0], [1], 1 when pressed
		Notes:
			Button presses exhibit a bouncing behavior, multiple message will be sent for a single button press.
			Additional buttons are also available past Button[5]

*/

using std::placeholders::_1;

class GoKartTeleop : public rclcpp::Node
{
public:
    GoKartTeleop() : Node("gokart_teleop")
    {

    RCLCPP_INFO(this->get_logger(), "Using default parameters if non are set in launch file");

    steering_axis_ =  this->declare_parameter<int>("steering_axis", 0);
    steering_scale_ =  this->declare_parameter<double>("steering_scale", 10.0);
    throttle_axis_ = this->declare_parameter<int>("throttle_axis", 5);
    throttle_scale_ = this->declare_parameter<double>("throttle_scale", 1.0);
    brake_axis_ = this->declare_parameter<int>("brake_axis", 2);
    brake_scale_ = this->declare_parameter<double>("brake_scale", 1.0);

    joy_sub_ = this->create_subscription<sensor_msgs::msg::Joy>(
        "/joy", 10, std::bind(&GoKartTeleop::joy_callback, this, _1));

    steering_pub_ = this->create_publisher<interfaces::msg::SteeringStamped>(
        "steering_wheel_angle", 10);

    throttle_brake_pub_ = this->create_publisher<interfaces::msg::ThrottleBrakeStamped>(
        "throttle_brake_effort", 10);

    }

private:

    void joy_callback(const sensor_msgs::msg::Joy::SharedPtr msg) const
    {
        interfaces::msg::SteeringStamped steering_msg;
        interfaces::msg::ThrottleBrakeStamped throttle_brake_msg;

        // Sign might be incorrect, test this
        steering_msg.steering_wheel_angle = steering_scale_ * msg->axes.at(steering_axis_);

        static bool gamepad_init = false;
        if(msg->axes.at(throttle_axis_) != 0 && msg->axes.at(brake_axis_) != 0 ) { gamepad_init = true; } // only needed if the a desired axis is a trigger

        if(gamepad_init)
        {
            if(msg->axes.at(brake_axis_) < 0.99)
            {
                throttle_brake_msg.propulsive_effort = brake_scale_*0.5*(msg->axes.at(brake_axis_) - 1); // map [1, -1] to brake_scale_*[0, -1]
            }
            else
            {
                throttle_brake_msg.propulsive_effort = -throttle_scale_*0.5*(msg->axes.at(throttle_axis_) - 1); // map [1, -1] to throttle_scale_*[0, 1]
            }
        }

        steering_pub_->publish(steering_msg);
        throttle_brake_pub_->publish(throttle_brake_msg);

    }

    rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr joy_sub_;
    rclcpp::Publisher<interfaces::msg::SteeringStamped>::SharedPtr steering_pub_;
    rclcpp::Publisher<interfaces::msg::ThrottleBrakeStamped>::SharedPtr throttle_brake_pub_;

    int steering_axis_, throttle_axis_, brake_axis_;
    double steering_scale_, throttle_scale_, brake_scale_;

};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<GoKartTeleop>());
  rclcpp::shutdown();
  return 0;
}
