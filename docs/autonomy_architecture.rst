Autonomy Architecture - WIP
===========================

The Auto-Kart will be a waypoint following autonomy. The optimal racing line will
be supplied aprior and the vehicle will attempt to track the provided racing
line. The architecture is general is an offshoot of the software architecture used by most teams 
during the DARPA grand and urban challenges

.. image:: images/preliminary_architecture_2_15_20.png
   :width: 640px
   :align: center
   :height: 400px
