# Go Kart Teleop 
This package is for controlling the steering, throttle, and brake actuators using a xbox controller. 

## Subscribes
* "/joy" : sensor_msgs::msg::Joy

## Publishes
* "/gokart_teleop/steering_wheel_angle" : interfaces::msg::SteeringStamped
* "/gokart_teleop/throttle_brake_effort" : interfaces::msg::ThrottleBrakeStamped