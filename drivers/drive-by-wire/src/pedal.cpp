#include <chrono>
#include <memory>
#include "interfaces/msg/throttle_brake_stamped.hpp"
#include "interfaces/msg/throttle_brake_stamped_sensor.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

class PedalNode : public rclcpp::Node
{
public:
  PedalNode()
  : Node("pedal_node")
  {

    //Subscribes to topic that takes in a value for throttle/brake system
    subscriberTBS_ = this->create_subscription<interfaces::msg::ThrottleBrakeStampedSensor>(
      "/throttle_brake_stamped_sensor", 10, std::bind(&PedalNode::topic_callback, this, _1));

    //Publishes the sensor data
    publisher_ = this->create_publisher<interfaces::msg::ThrottleBrakeStamped>("/throttle_brake_effort", 10);

  }

private:
  void topic_callback(const interfaces::msg::ThrottleBrakeStampedSensor::SharedPtr msg)
  {
    // Get the command and store it and toggle a flag.
    RCLCPP_INFO(this->get_logger(), "I heard: '%i'", msg->accelerator_pedal);
    controlCommand = (msg->accelerator_pedal - minValue)/(maxValue - minValue) - 1.0/2;

    auto message = interfaces::msg::ThrottleBrakeStamped();
    message.header.stamp = get_clock()->now();
    message.propulsive_effort = controlCommand;
    publisher_->publish(message);
    
  }

  // -------------- Variables ---------------
  rclcpp::Subscription<interfaces::msg::ThrottleBrakeStampedSensor>::SharedPtr subscriberTBS_;
  rclcpp::Publisher<interfaces::msg::ThrottleBrakeStamped>::SharedPtr publisher_;
  float controlCommand;
  float minValue = 230.0; //TODO: obtain actual min value
  float maxValue = 300.0; //TODO: obtain actual max value
};

int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<PedalNode>());
  rclcpp::shutdown();
  return 0;
}
