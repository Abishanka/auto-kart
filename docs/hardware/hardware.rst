Hardware-WIP
============
Here there is usage instructions for different hardware on the Auto-Kart

Electronics Documentation
-------------------------
TODO

Mechanical Documentation
------------------------
TODO

Mechatronics Documentation
--------------------------

Actuation
*********
.. toctree::
   :maxdepth: 1

   Animatics SmartMotor <hwdocs/smartmotor>

Sensors
*******
.. toctree::
   :maxdepth: 1

   Sylphase-Infix-1 <hwdocs/sylphase>
   Velodyne VLP-16 <hwdocs/velodyne>
