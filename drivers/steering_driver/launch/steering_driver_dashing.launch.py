from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    ld = LaunchDescription()
    
    steering_driver_node = Node(
        package="steering_driver",
        node_executable="steering_driver"
    )

    ld.add_action(steering_driver_node)

    return ld
