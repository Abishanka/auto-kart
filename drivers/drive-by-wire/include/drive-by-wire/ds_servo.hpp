#include <math.h>
#include "driver.hpp"

#define ADC_TO_PSI      20.3    // Place holder
#define COUNT_TO_RPM    6000    // rev/min/count
#define ADC_TO_DEG      1       // Place holder
/*
 * This driver is for use with an Arduino Mega controlling a DS3230MG RC Servomotor
 * This driver could not function as intended if the corresponding Arduino is not running
 * on the Arduino Mega.
 * 
 * TODO:
 *      * Sensor data from Arduino needs to get out of the class so the main program can publish the 
 *          data over ROS.
 * 
 * Notes:
 *      Servo Motor Parameters: Pulse width range:  500-2500 microseconds
 *                              Neutral Position:   1500 microseconds
 *                              Working Frequency:  50-333 Hz
 *                              Rotating CC:        1000-2000 microseconds
 */
class DSServoDriver : public ActuationDriver
{
    private:
        uint16_t command = 0;
        bool newEffort = false;
        //|          0      |         1        |           2          |       3        |
        //| brake Pressure  | rear wheel speed | steering wheel angle | pedal velocity |
        //|     PSI/kPa     |       RPM        |    Degrees/Radians   |     m/s        |
        double sensorData[4] = {0}; // Consider a standard array
        bool invalid_arg = false;
        bool firstTime = true;

    public:
        DSServoDriver(std::string port, unsigned long baud, int timeout_ms)
            : ActuationDriver(port, baud, timeout_ms)
        {

        }

        void sendMessage() override
        {
            // Arduino expects a message with a 2 byte number and a CRC byte. 3 Total bytes
            // Number should range between 0 and 1023 based on desired motor angle.
            uint8_t data[3] = {0};

            if(isOpen())
            { 
                data[0] = command & 0xFF;
                data[1] = (command >> 8) & 0xFF;
                data[2] = calculate_crc(data, 2);
                write(data, 3); 
            }
        }

        std::string driverStatus() override
        {
            std::string status;

            if(!invalid_arg){ status = "Drivers have run"; }
            else { status = "Invalid Argument"; invalid_arg = false; }

            return status;
        }

        void updateEffort(double effort) override
        {
            // Input should be a int between [0, 1]
            if(effort < 0 || effort > 1)
            { 
                invalid_arg = true;
                return;
	        }

            // 0->1 should map between 60->90, these numbers are used to control the servomotor
            // Currently 90 represents min throttle and 60 represents max throttle
            command = (uint16_t)(-30*effort + 90);
            newEffort = true;

        }

        void readIncomingMessage() override
        {
            // Received message will contain chassis sensor data:
            // brake_pressure, back wheel count, steering, pedal, crc.
            // None of these quantities have appropriate units. They must be converted.
            // They are all also uint16_t variables that have been split into two seperate bytes
            uint8_t data[9] = {0};

            // TODO: Might need a loop to read all available data from the buffer and only use the last complete message.
            // The arduino will publish message at 100Hz and the time between .update() calls could be larger than that leading
            // to the buffer being filled up.
            if(read(data, 9) == 9)
            {
                if(data[8] == calculate_crc(data, 9))
                {
                    sensorData[0]   = (double)((uint16_t)(data[1] << 8 | data[0]))*ADC_TO_PSI; // TODO: Is this the correct conversion?
                    // Wheel speed data is not optimal, can often be zero at low speeds. Will need to have some kind of moving average
                    // to prevent the wheel speed from fluctuating too much
                    sensorData[1]  = (double)((uint16_t)(data[3] << 8 | data[2]))*COUNT_TO_RPM;
                    sensorData[2] = (double)((uint16_t)(data[5] << 8 | data[4]))*ADC_TO_DEG; // TODO: Conversion needs to be updated
		            sensorData[3] = (double)((uint16_t)(data[7] << 8 | data[6]));
                }
            }
        }

        uint8_t calculate_crc(uint8_t* data, int data_len)
        {
            uint8_t crc = 0;
            for (int i = 0; i < data_len; i++)
            {
                crc ^= data[i];
            }
            return crc;
        }


        void update() override
        {

            if(isOpen())
            {
                // Don't try to read if nothing is there.
                if(available())
                {
                    readIncomingMessage();
                }
                
                if(newEffort)
                {
                    sendMessage();
                    newEffort = false;
                }
            }
        }

        double* getData()
        {
            return sensorData;
        }

};

