Getting Started
===============
If it is your first time working with the repository, follow the instructions in Setup to setup
your system.

Useful Resources
----------------
- `ROS2 Wiki <https://index.ros.org/doc/ros2/>`_
- Google

Software Development Guides
---------------------------
.. toctree::
   :maxdepth: 1

   Setup <setup.md>
   Working with Documentation <documentation.md>
   Creating a new package <new_package.md>
   Git Tips <git_tips.md>

