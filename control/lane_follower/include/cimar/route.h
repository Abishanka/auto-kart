#ifndef ROUTE_H
#define ROUTE_H

// This structure is used to define a single point on the route and the entire route
// is an array of these structures.  The code assumes that the route is planar (xy plane).
struct route_point_struct
{
	double pt[3] ; // the x,y,z coordinates of the point ; has units of length as defined by user
	double heading_rad ; // heading about the z axis, 0 is along the x axis, pi/2 is along the y axis
	double w1_for_subsequent_segment ;  // this point is the start of the subsquent route segment and
                                        // w1 is the desired value for that segment (typically equals 1)
	double speed ;  // can set a desired speed for the subsequent route segment; units are length_unit/sec
} ;


// This structure defines a single route segment which is defined by three points and the value 'w1'.
// The length of the segment is calculated and stored.  A flag is set if the segment is perfectly straight.
// The entire route is defined as an array of these route segments.
struct route_segment_struct
{
	double p0[3] ;  // the initial point of the segment ; has units of length as defined by user
	double p1[3] ;  // the intermediate control point for the segment
	double p2[3] ;  // the end point of the segment
	double w1 ;     // the scalar shape parameter for the segment
	double length ; // the distance length of this route segment
	int is_straight_seg ;  // set to 1 if the segment is perfectly straight ; set to 0 otherwise
} ;

// function prototypes of functions in route.cpp

void   create_route_segments(struct route_segment_struct route_segments[], struct route_point_struct route_points[], int num_points) ;
	// inputs -
	//      route_points:  an array of route_point_struct (point on route, heading at that point, w1 for subsequent segment)
	//      num_pts: number of points on route + 1 (the first point is repeated as the last point for continuous loop)
	// outputs -
	//      route_segments:  an array of route_segment_struct (p0, p1, p2, w1, length, is_straight)
	//                       Note that the number of route_segments is one less than the number of points for a continuous loop.

void   get_pt_on_route(double pt[3], struct route_segment_struct *route_segment, double u) ;
	// inputs -
	//      route_segment: a pointer to a struct route_segment
	//      u: the value of u along the route segment, 0 <= u <= 1
	// outputs -
	//      pt: the coordinates of the point on the route segment

double get_route_length_to_u(struct route_segment_struct *route_segment, double u) ;
	// inputs -
	//     route_segment: pointer to a route_segment_struct
	//     u: the value of u on the route segment, 0 <= u <= 1
	// return - distance from point p0 of segment to the point designated by u

double get_uval_for_dist(struct route_segment_struct *route_segment, double dist) ;
	// inputs -
	//     route_segment: pointer to a route_segment_struct
	//     dist: a distance along the path from the point p0
	// return - value of u that corresponds to the point that is dist away from p0


void   get_slope_and_curvature_on_route(double *dxdu, double *dydu, double *radius, struct route_segment_struct *segment, double u) ;
	// inputs -
	//    segment: pointer to a route_segment_struct
	//    u: value of u corresponding to a certain point on the segment, 0 <= u <= 1
	// outputs -
	//    dxdu: pointer to derivative of x with respect to u at point on segment designated by value of u
	//    dydu: pointer to derivative of y with respect to u at point on segment designated by value of u
	//    radius: radius of curvature at point on segment designated by value of u


#endif
