# Setup
## OS Requirements

The first step is to get a compatabile OS; we use Ubuntu 20.04. This can be accomplished 3 ways:

- Running Ubuntu natively on your computer.
- Running Ubuntu on a virtual machine on your Windows or MacOS native machine
- Dual booting your Windows/MacOS machine with Ubuntu 20.04

## Software Dependencies
- ROS2 Foxy
- Git
- Sphinx (1.6.7+)
- pip3

## Installing Dependencies
You will need to install these before you can work with the Auto-Kart code.

### ROS2 Foxy
Install [ROS2 Foxy](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/) on your system.

You will also need to install colcon before building the workspace.
```bash
sudo apt install python3-colcon-common-extensions
```

You should also install rosdep to help manage the repo dependcies
```bash
sudo apt install python3-rosdep2
```

After it is succesfully installed, run the command below and follow the prompt after it completes
```bash
sudo rosdep init
```

### Sphinx with readthedocs
You would only need to install Sphinx if you plan on creating/editing documentation for the project. However, if you create any packages you should also be creating appropriate documentation for them. 

```Bash
sudo apt install python3-sphinx
```
Install pip3
```Bash
sudo apt install python3-pip
```
Use pip3 to install the Read the Docs template
```Bash
pip3 install sphinx-rtd-theme
```
Use pip3 to install recommommark to be able to use markdown files
```Bash
pip3 install recommonmark
```

## Creating a workspace and building the repository
You can follow the [guide](https://index.ros.org/doc/ros2/Tutorials/Workspace/Creating-A-Workspace/) from the offical ROS2 wiki to create a workpace to place the auto-kart repository. Replace the git clone commands in step 3 with:
```bash
git clone https://gitlab.com/navigator-agk/auto-kart.git
```
The repo relies on submodules for some of the functionality. Run the command below in the root directory of the git repository (not the ROS workspace) to pull these submodules into your local repository.
```bash
git submodule update --init --recursive
```

Then run the following command to get all of the dependencies
```bash
rosdep install -i --from-path src --rosdistro foxy -y
```

Before attempting to build the workspace, you will need to source the ROS2 software if you haven't included it already in .bashrc
```bash
source /opt/ros/foxy/setup.bash
```
if you have not added the command to the .bashrc, you will need to do this for each terminal instance you use 

Navigate back to the root directory of the workspace then run
```bash
colcon build
```