import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    rviz_config = os.path.join(
        get_package_share_directory('lane_follower'),
        'config',
        'cdc_indy_rviz.rviz'
    )

    return LaunchDescription([
        Node(
            package='lane_follower',
            namespace='lane_follower',
            executable='optimal_route',
            name='optimal_route'
        ),
        Node(
            package='lane_follower',
            namespace='lane_follower',
            executable='vehicle_simulator',
            name='vehicle_simulator'
        ),
        Node(
            package='lane_follower',
            namespace='lane_follower',
            executable='get_carrot',
            name='get_carrot',
            output='screen' 
        ),
        Node(
            package='lane_follower',
            namespace='lane_follower',
            executable='vehicle_control',
            name='vehicle_control',
            output='screen' 
        ),
        Node(
            package='lane_follower',
            namespace='lane_follower',
            executable='draw_path',
            name='draw_path',
            output='screen' 
        ),
        Node(
            package='tf2_ros',
            namespace='tf2_ros',
            executable='static_transform_publisher',
            name='cdc_tf',
            arguments=["0", "0", "0", "0", "0", "0", "map", "my_frame"]
        ),
        Node(
            package='rviz2',
            namespace='rviz2',
            executable='rviz2',
            name='rviz2',
	    arguments=["-d",rviz_config]
        ),
    ])

