#ifndef PURE_PURSUIT
#define PURE_PURSUIT

// function prototypes for functions in pure_pursuit.cpp

void get_look_ahead_point(double look_ahead_pt[3],
                          double *look_ahead_heading_rad,
                          double my_closest_pt_lines[3],
                          int *my_closest_segment,
                          double my_closest_pt_curve[3],
                          double *heading_closest_pt_curve,
                          double look_ahead_dist,
                          double vehicle_pt[3],
                          int num_segments,
                          struct route_segment_struct route_segments[]) ;
	// inputs -
	//     look_ahead_dist: the distance from the closest point on the route to the look ahead point
	//     vehicle_pt: coordinates of the vehicle at this instant
	//     num_segments: number of route segments
	//     route_segments: an array of route_segment_struct
	//
	// outputs -
	//     look_ahead_pt: coordinates of point that is 'look_ahead_dist' from the closest point on the curved route
	//     look_ahead_heading_rad: pointer to direction tangent to route at the look ahead point, measured from x axis
	//     my_closest_pt_lines:  the point on the straight line segments that is closest to the vehicle point
	//     *my_closest_segment :  the segment number on which the point on the straight line segments that is closest to the vehicle point lies
	//     my_closest_pt_curve:  the point on the curved route that is closest to the vehicle point
        //     heading_closest_pt_curve:  the headging angle in radians at the closest point on the route segment 


double get_radius(double vehicle_pt[3],
                  double vehicle_heading,
                  double look_ahead_pt[3],
                  double look_ahead_heading,
                  double d1, double w1) ;
	// A path from the current vehicle location to the look ahead point is created from two route
	// segments.  See section 5 (Determination of Instantaneous Steering Angle) of the Route Planning
	// Document.  The radius of curvature at the start of the first route segment is calculated and
	// is returned by this function.
	
	// inputs -
	//     vehicle_pt: coordinates of the vehicle at this instant
	//     vehicle_heading: current orientation of the vehicle (radians) from the x axis, about the z axis
	//     look_ahead_pt: coordinates of the look ahead point
	//     look_ahead_heading: desired orientation at the look ahead point
	//     d1:  A distance used to calculate points M1 and M2 (see Route Planning Document).  Typically
	//          set equal to one fourth of the look ahead distance that was used to get the look ahead
	//          point.
	//     w1:  A dimensionless scalar that defines the shape of the route segments.  Typically equals 1.
	//
	// outputs -
	//     The radius of curvature at the start of the path is returned.  A positive value means turn left
	//     (need to double check this).  A value of 9999.0 is returned if the vehicle should go straight
	//     ahead, i.e. maintain its current orientation.

				  
// Note: There are other functions defined in pure_pursuit.cpp, but they are support functions for the two
//       functions listed above.

#endif

