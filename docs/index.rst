Home
====
Welcome to Auto-Kart's documentation. Explore the links below to learn more about the
team and the software.

.. figure:: images/navigator_agk.jpg
  :scale: 70%
  :align: center
  :alt: A picture of the NaviGator autonomous go-kart

  NaviGator AGK, photo by David Damiani

Collaborators
-------------

`Center for Intelligent Machines and Robotics <https://cimar.mae.ufl.edu/>`_, CIMAR

`Machine Intelligence Laboratory <https://www.mil.ufl.edu/>`_, MIL

Links
-----

.. toctree::
   :maxdepth: 2

   Development Tools <development_tools>
   Getting Started <development/getting_started>
   Autonomy Architecture <autonomy_architecture>
   Software <software/software>
   Hardware <hardware/hardware>
